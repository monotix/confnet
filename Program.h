//
//  Program.h
//  
//
//  Created by Rafał Osowicki on 21.07.2015.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Program : NSManagedObject

@property (nonatomic, retain) NSDate * dateProgram;
@property (nonatomic, retain) NSString * dayHuman;

@end
