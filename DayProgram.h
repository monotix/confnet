//
//  DayProgram.h
//  
//
//  Created by Rafał Osowicki on 21.07.2015.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Program;

@interface DayProgram : NSManagedObject

@property (nonatomic, retain) NSString * detailValue;
@property (nonatomic, retain) NSString * hours;
@property (nonatomic, retain) Program *dateProgram;

@end
