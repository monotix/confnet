//
//  TabBarController.h
//  Confnet
//
//  Created by Rafał Osowicki on 15.07.2015.
//  Copyright (c) 2015 Rafał Osowicki. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TabBarController : UIView<UITabBarControllerDelegate>


@end
