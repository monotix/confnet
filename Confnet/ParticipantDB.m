//
//  ParticipantDB.m
//  Confnet
//
//  Created by Rafał Osowicki on 03.08.2015.
//  Copyright (c) 2015 Rafał Osowicki. All rights reserved.
//

#import "ParticipantDB.h"


@implementation ParticipantDB

@dynamic avatarURL;
@dynamic company;
@dynamic companyPosition;
@dynamic email;
@dynamic name;
@dynamic paid;
@dynamic phone1;
@dynamic phone2;
@dynamic userID;

@end
