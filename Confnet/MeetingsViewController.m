//
//  MeetingsViewController.m
//  Confnet
//
//  Created by Rafał Osowicki on 02.07.2015.
//  Copyright (c) 2015 Rafał Osowicki. All rights reserved.
//

#import "MeetingsViewController.h"
#import "CellController.h"
#import "JsonManager.h"
#import "Meeting.h"
#import "Participants.h"
#import "DetailViewController.h"

@implementation MeetingsViewController
static NSString *CellIdentifier= @"MeetingsCell";
@synthesize tableView;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIImage* image3 = [UIImage imageNamed:@"ic_action_logout.png"];
    CGRect frameimg = CGRectMake(15,5, 30,30);
    UIButton *logoutButton = [[UIButton alloc] initWithFrame:frameimg];
    [logoutButton setBackgroundImage:image3 forState:UIControlStateNormal];
    [logoutButton addTarget:self action:@selector(Logout)
           forControlEvents:UIControlEventTouchUpInside];
    [logoutButton setShowsTouchWhenHighlighted:YES];
    
    UIBarButtonItem *logoutButton1 =[[UIBarButtonItem alloc] initWithCustomView:logoutButton];
    UINavigationItem *navItem = [[UINavigationItem alloc] init];
    navItem.rightBarButtonItem = logoutButton1;
    navItem.title = @"Meetings";
    super.bar.items = @[ navItem ];
    
    refreshControll = [[UIRefreshControl alloc] init];
    [refreshControll setTintColor:[UIColor colorWithRed:217.0f/255.0f green:11.0f/255.0f blue:140.0f/255.0f alpha:1.0]];
    [refreshControll tintColorDidChange];
    [tableView addSubview:refreshControll];
    [refreshControll addTarget:self action:@selector(MeetingsInit) forControlEvents:UIControlEventValueChanged];
    
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeLeft:)];
    swipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipeLeft];
    swipeLeft.delegate = self;
    
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRight:)];
    swipeRight.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swipeRight];
    swipeRight.delegate = self;
    
    _activityindicator = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(150, 200, 30, 30)];
    [_activityindicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [_activityindicator setColor:[UIColor colorWithRed:217.0f/255.0f green:11.0f/255.0f blue:140.0f/255.0f alpha:1.0]];
    _activityindicator.center = self.view.center;
    
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    [self.view addSubview:_activityindicator];
    
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView = (id)[self.view viewWithTag:3];
    [tableView registerClass:[CellController class] forCellReuseIdentifier:CellIdentifier];
    tableView.backgroundColor = [UIColor colorWithRed:0.93 green:0.93 blue:0.93 alpha:1.0];
    [tableView addSubview:refreshControll];
    
    UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 70)];
    footer.backgroundColor = [UIColor clearColor];
    tableView.tableFooterView = footer;
    [tableView reloadData];
    
   
    
    tableView.allowsSelection = NO;
    
    DBM = [DBManager sharedManager];
    JManager = [[JsonManager alloc] init];
    
    if(![DBM hasConnectivity]){
        if(!ListDone){
        
            
        ParticipantList = [[NSMutableArray alloc] init];
        MeetingsList = [[NSMutableArray alloc]init];
        MeetingsList = [DBM loadMeetingsFromDB];
        ParticipantListTemp = [[NSMutableArray alloc] init];
        ParticipantListTemp = [DBM loadParticipantsFromDB];
        imageList = [[NSMutableArray alloc] init];
        NSLog(@"Tabele: %d %d",[MeetingsList count],[ParticipantListTemp count]);
        for (int j = 0; j < [MeetingsList count]; j++) {
            
        for (int i = 0 ; i < [ParticipantListTemp count]; i++) {
            if ([[[MeetingsList objectAtIndex:j] usrID] isEqualToString:[[ParticipantListTemp objectAtIndex:i] usrID]]) {
                [ParticipantList addObject:[ParticipantListTemp objectAtIndex:i]];
                //[imageList addObject:[DBM loadImage:[NSString stringWithFormat:@"%d.png",i]]];
                

                
                ListDone = YES;
                [tableView addSubview:refreshControll];
            }
            }
        }
            
        }
        NSLog(@"Tabele: %d %d",[MeetingsList count],[ParticipantList count]);
      
             [tableView reloadData];
    }else{

    
    if(!ListDone){
        //MeetingsList = [[NSMutableArray alloc] init];
        //ParticipantList = [[NSMutableArray alloc] init];
        imageList = [[NSMutableArray alloc] init];
        [self MeetingsInit];
    }}
    
}

-(void)MeetingsInit{
    
    if([DBM hasConnectivity]){
        
    [DBM deleteMeetingsFromDB];
        
        if(!ListDone){
        [_activityindicator startAnimating];
            [tableView setUserInteractionEnabled:NO];
        }
        ParticipantListTemp = [[NSMutableArray alloc] init];
        MeetingsListTemp = [[NSMutableArray alloc] init];
        [MeetingsListTemp removeAllObjects];
        [ParticipantListTemp removeAllObjects];
    [tableView setUserInteractionEnabled:NO];
    //NSString *address = [NSString stringWithFormat:@"http://10.0.1.15/wp-json/cn/meetings"];
    //NSString *address2 = [NSString stringWithFormat:@"http://10.0.1.15/wp-json/cn/participants/detail/"];
    NSString *address = [NSString stringWithFormat:@"http://polanditmeeting.com/wp-json/cn/meetings"];
    NSString *address2 = [NSString stringWithFormat:@"http://polanditmeeting.com/wp-json/cn/participants/detail/"];
    //ParticipantList = [[NSMutableArray alloc] init];
    
        token = [DBM loadTokenFromDB];
        [JManager getJSONMeetingsByAddress:address WithToken:token Completion:^(BOOL success) {
            if(success){
            muid = [[NSMutableArray alloc] init];
            for (int i = 0; i < [JManager getMeetingsCount]; i++) {
                
                Meeting *m = [[Meeting alloc] initWithMid:[JManager getMeetingId:i] InviteState:[JManager getInviteState:i] Time:[JManager getTime:i]Message:[JManager getMessage:i]UserID:[JManager getMeetingUserId:i]];
                [MeetingsListTemp addObject:m];

                [JManager getJSONByAddress:address2 WithToken:token UserID:[m usrID] Completion:^(BOOL success) {
                    if(success){
                        sc = NO;

                        
                    
                    Participants *p = [[Participants alloc] initWithUserName:[JManager getUserName] UserID:[m usrID] Email:[JManager getUserEmail] Company:[JManager getUserCompany] CompanyPosition:[JManager getcompanyPosition] Phone1:[JManager getUserPhone1] Phone2:[JManager getUserPhone2] UserPaid:[JManager getUserPaid] AvatarURL:[JManager getUserAvatar] Questionnaire:nil Keys:nil];
                        
                        quest = [[NSMutableDictionary alloc] init];
                        keys = [[NSMutableArray alloc] init];
                        for (int j = 0; j < [JManager getQuestCount]; j++) {
                            
                            [quest setObject:[JManager getValueArIndex:j] forKey:[JManager getDescriptionAtIndex:j]];
                            [keys addObject:[JManager getDescriptionAtIndex:j]];
                        }
                        p.questionnaire = quest;
                        p.keys = keys;
                    
                [ParticipantListTemp addObject:p];
                    
                        
                    [tableView setUserInteractionEnabled:YES];
                    
                    
                    
                    ParticipantList = [[NSMutableArray alloc] initWithArray:ParticipantListTemp];
                        MeetingsList = [[NSMutableArray alloc] initWithArray:MeetingsListTemp];
                    [DBM saveMeetingsToDB:MeetingsList];
                    [_activityindicator stopAnimating];
                    [refreshControll endRefreshing];
                        [UIView transitionWithView:self.tableView
                                          duration:0.5f
                                           options:UIViewAnimationOptionTransitionCrossDissolve
                                        animations:^(void)
                         {
                             [tableView reloadData];
                         }
                                        completion:nil];
                        
                    ListDone = YES;
                    if(!ListDone)
                        [self refreshTable];
                    
                    [tableView setUserInteractionEnabled:YES];
                    }else{
                        NSLog(@"No success");
                    }
                }];
                    //NSLog(@"uczestnik: %@, POZ %@",[[MeetingsList objectAtIndex:j] usrID],[[ParticipantListTemp objectAtIndex:j] usrID]);

                }
                
            
            }else{
               
                    [_activityindicator stopAnimating];
                [refreshControll endRefreshing];
                    [tableView reloadData];
                sc = YES;
                    return;
                }
            
         [tableView setUserInteractionEnabled:YES];
            
        }];
        
    }else{
        [refreshControll endRefreshing];
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Network" message: @"No internet connection." delegate: self
                                             cancelButtonTitle:@"OK"
                                             otherButtonTitles:nil,nil];
        
        [alert show];
        [tableView setUserInteractionEnabled:YES];
        
        
    }
    
}

- (void)refreshTable {
    [refreshControll endRefreshing];
    [_activityindicator startAnimating];
    [tableView reloadData];
}

-(void)refreshData:(UITableView *)t{
    [t reloadData];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if ([ParticipantList count] != 0) {
        
        self.tableView.backgroundView = nil;
        return [ParticipantList count];
        
    } else {
        
        UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
        
        NSString *text;
        if([DBM hasConnectivity] && [[JManager getError] isEqualToString:@"-1011"])
            text = [NSString stringWithFormat:@"You must pay first to see this content."];
        else
            text = [NSString stringWithFormat:@"No data is currently available. Please pull down to refresh."];
        
        messageLabel.text = text;
        messageLabel.textColor = [UIColor blackColor];
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = NSTextAlignmentCenter;
        messageLabel.font = [UIFont systemFontOfSize:16];
        [messageLabel sizeToFit];
        
        self.tableView.backgroundView = messageLabel;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CellController *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    _screenBound = tableView.frame;
    CGRect bd = [[UIScreen mainScreen] bounds];
    
    if(cell == nil){
        cell = [[CellController alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        NSLog(@"nil");
    }else{
        cell.layer.shadowOffset = CGSizeMake(1, 0);
        cell.layer.shadowColor = [[UIColor blackColor] CGColor];
        cell.layer.shadowRadius = 5;
        cell.layer.shadowOpacity = .25;
        NSLog(@"Sraka taka %@",[[MeetingsList objectAtIndex:indexPath.section] time]);
        cell.time = [[MeetingsList objectAtIndex:indexPath.section] time];
        cell.status = [[MeetingsList objectAtIndex:indexPath.section] inviteState];
        for (int i = 0; i < [ParticipantList count]; i++) {
            if ([[[ParticipantList objectAtIndex:i] usrID] isEqualToString:[[MeetingsList objectAtIndex:indexPath.section] usrID]]) {
        cell.userName = [[ParticipantList objectAtIndex:i] userName];
                
                if(!([[ParticipantList objectAtIndex:i] company] == nil)){
                    cell.company = [NSString stringWithFormat:@"%@",[[ParticipantList objectAtIndex:i] company]];
                    //NSLog(@"tutaj %@", participant.company);
                }
                if(![[[ParticipantList objectAtIndex:i] companyPosition] isEqualToString:@"<null>"] && ![[[ParticipantList objectAtIndex:i] companyPosition] isEqualToString:@"(null)"] && !([[ParticipantList objectAtIndex:i] companyPosition] == nil) && ![[[ParticipantList objectAtIndex:i] companyPosition] isEqualToString:@""])
                    cell.company = [NSString stringWithFormat:@"%@, %@",[[ParticipantList objectAtIndex:i] companyPosition],[[ParticipantList objectAtIndex:i] company]];
                
        //cell.company = [NSString stringWithFormat:@"%@, %@",[[ParticipantList objectAtIndex:i] companyPosition],[[ParticipantList objectAtIndex:i] company]];
    
        
        cell.avatarIMG.image = [DBM loadImage:[NSString stringWithFormat:@"%@.png",[[ParticipantList objectAtIndex:i] usrID]]];
                
                UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
                [button addTarget:self
                           action:@selector(showDetail:)
                 forControlEvents:UIControlEventTouchDown];
                [button setTitle:@"USER DETAILS" forState:UIControlStateNormal];
                button.titleLabel.font = [UIFont boldSystemFontOfSize:13];
                [button setTintColor:[UIColor blackColor]];
                button.frame = CGRectMake(_screenBound.origin.x+bd.size.width*0.02, _screenBound.origin.y+50, 120, 30);
                [button setTag:i];
                [cell.contentView addSubview:button];
                
                
        }
        }
        
        UIButton *button1 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [button1 addTarget:self
                    action:@selector(showMessage:)
          forControlEvents:UIControlEventTouchDown];
        [button1 setTitle:@"SHOW MESSAGE" forState:UIControlStateNormal];
        button1.titleLabel.font = [UIFont boldSystemFontOfSize:13];
        [button1 setTintColor:[UIColor blackColor]];
        button1.frame = CGRectMake(_screenBound.origin.x+bd.size.width*0.4, _screenBound.origin.y+50, 120, 30);
        [button1 setTag:indexPath.section];
        [cell.contentView addSubview:button1];
        
        
    }
    
    return cell;
}

-(NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    return indexPath;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)showDetail:(UIButton* )sender{

    DetailViewController *NVC = [self.storyboard instantiateViewControllerWithIdentifier:@"Detail"];
    [NVC setParticipant:[ParticipantList objectAtIndex:[sender tag]]AndIndex:[sender tag]Image:nil PartList:ParticipantList];
    [self presentViewController:NVC animated:YES completion:nil];
}

-(void)showMessage:(UIButton *)sender{
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Message" message: [[MeetingsList objectAtIndex:[sender tag]] msg] delegate: self
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil,nil];
    
    [alert show];
    
}

-(void) swipeRight:(UISwipeGestureRecognizer *) recognizer {
    if (recognizer.direction == UISwipeGestureRecognizerDirectionRight)
        [self.tabBarController setSelectedIndex:[self.tabBarController selectedIndex] - 1];
}

-(void) swipeLeft:(UISwipeGestureRecognizer *) recognizer {
    if (recognizer.direction == UISwipeGestureRecognizerDirectionLeft)
        [self.tabBarController setSelectedIndex:[self.tabBarController selectedIndex] + 1];
    
}



@end
