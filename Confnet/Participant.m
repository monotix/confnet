//
//  Participant.m
//  Confnet
//
//  Created by Rafał Osowicki on 03.08.2015.
//  Copyright (c) 2015 Rafał Osowicki. All rights reserved.
//

#import "Participant.h"
#import "QuestionAnswer.h"


@implementation Participant

@dynamic avatarURL;
@dynamic company;
@dynamic company_position;
@dynamic email;
@dynamic phone1;
@dynamic phone2;
@dynamic user_name;
@dynamic user_paid;
@dynamic userID;
@dynamic user_id;

@end
