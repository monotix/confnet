//
//  ValueAnswer.h
//  Confnet
//
//  Created by Rafał Osowicki on 03.08.2015.
//  Copyright (c) 2015 Rafał Osowicki. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class QuestionAnswer;

@interface ValueAnswer : NSManagedObject

@property (nonatomic, retain) NSString * value;
@property (nonatomic, retain) QuestionAnswer *questionID;

@end
