//
//  DayProgram.m
//  Confnet
//
//  Created by Rafał Osowicki on 03.08.2015.
//  Copyright (c) 2015 Rafał Osowicki. All rights reserved.
//

#import "DayProgram.h"


@implementation DayProgram

@dynamic date;
@dynamic detailValue;
@dynamic hours;

@end
