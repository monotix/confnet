//
//  DBManager.h
//  Confnet
//
//  Created by Rafał Osowicki on 06.07.2015.
//  Copyright (c) 2015 Rafał Osowicki. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"
#import "JsonManager.h"
#import "Participants.h"
#import <SystemConfiguration/SystemConfiguration.h>
#import <sys/socket.h>
#import <netinet/in.h>

@interface DBManager : NSObject{
    AppDelegate *appDelegate;
    NSManagedObjectContext *context;
    JsonManager *JManager;
    Participants *par;
    BOOL offline;
}

+(id)sharedManager;
-(void)saveTokenToDB:(NSString*)tkn;
-(NSString*)loadTokenFromDB;
-(void)saveParticipantsToDB:(NSMutableArray *) parList;
-(NSMutableArray *)loadParticipantsFromDB;
-(BOOL)internetConnection;
-(void)setinternetConnection:(BOOL)b;
-(NSMutableArray *)loadMeetingsFromDB;
-(void)saveMeetingsToDB:(NSMutableArray *)meetingsList;
-(void)saveProgramToDB:(NSMutableArray *)programList;
-(NSMutableArray *)loadProgramFromDB;
-(void)deleteParticipanFromDB;
-(void)deleteMeetingsFromDB;
-(void)deleteProgramFromDB;
-(void)deleteToken;
-(BOOL)hasConnectivity;
-(void)saveImage: (UIImage*)image WithName:(NSString*)name;
-(UIImage*)loadImage:(NSString*)name;
-(void)removeImg;


@end
