//
//  WebViewController.h
//  Confnet
//
//  Created by Rafał Osowicki on 22.07.2015.
//  Copyright (c) 2015 Rafał Osowicki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBManager.h"
#import "JsonManager.h"

@interface WebViewController : UIViewController<UIWebViewDelegate>{
    DBManager *DBM;
    JsonManager *JManager;
    NSString *urlLink;
    NSString *token;
}

@property (strong, nonatomic) IBOutlet UIWebView *webView;

@end
