//
//  JsonManager.m
//  Confnet
//
//  Created by Rafał Osowicki on 06.07.2015.
//  Copyright (c) 2015 Rafał Osowicki. All rights reserved.
//

#import "JsonManager.h"
#import "AFNetworking/AFNetworking.h"

@implementation JsonManager

@synthesize JsonMsg, fullDate, dateTitle,questionnaire, JsonMsgDetail;
@synthesize programs,programListCount;

-(id)initWithAddress:(NSString*)addr{
    
    if(self){
    }
    
    return self;
}

-(void)getJSONByAddress:(NSString *)addr Completion:(void (^)(BOOL))completion{
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [manager setResponseSerializer:[AFJSONResponseSerializer serializer]];
    [manager POST:addr
       parameters:nil
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              JsonMsg = [responseObject valueForKey:@"program"];
              programs = [JsonMsg objectAtIndex:0];
              programListCount = [programs count];
                  completion(YES);
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(@"Error: %@", error);
              err = [NSString stringWithFormat:@"%ld", (long)[error code]];
              completion(NO);
          }];
}

-(void)getJSONByAddress:(NSString *)addr Login:(NSString *)login Password:(NSString *)pass Completion:(void (^)(BOOL))completion{
    
   
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [manager setResponseSerializer:[AFJSONResponseSerializer serializer]];
    [manager POST:addr
       parameters:@{@"email":login, @"password":pass}
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              token = [[[responseObject valueForKey:@"auth_data"] valueForKey:@"token"] objectAtIndex:0];
              NSLog(@"%@",token);
              completion(YES);
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(@"Error: %@", error);
              err = [NSString stringWithFormat:@"%ld", (long)[error code]];
              completion(NO);
          }];
}

-(void)getJSONByAddress:(NSString *)addr WithFacebookToken:(NSString *)tkn Completion:(void (^)(BOOL))completion{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [manager setResponseSerializer:[AFJSONResponseSerializer serializer]];
    [manager POST:addr
       parameters:@{@"token":tkn}
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              JsonMsg = [[responseObject valueForKey:@"participants"] objectAtIndex:0];
              token = [[[responseObject valueForKey:@"auth_data"] valueForKey:@"token"] objectAtIndex:0];
              //JsonMsg = [[responseObject valueForKey:@"participants"]valueForKey:@"user_name"];
              //NSLog(@"%@/n ilosc: %d",JsonMsg,[JsonMsg count]);
              completion(YES);
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(@"Error: %@", error);
              err = [NSString stringWithFormat:@"%ld", (long)[error code]];
              completion(NO);
          }];
    
}

-(void)getJSONByAddress:(NSString *)addr WithLinkedinToken:(NSString *)tkn Completion:(void (^)(BOOL))completion{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [manager setResponseSerializer:[AFJSONResponseSerializer serializer]];
    [manager POST:addr
       parameters:@{@"token":tkn}
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              JsonMsg = [[responseObject valueForKey:@"participants"] objectAtIndex:0];
              token = [[[responseObject valueForKey:@"auth_data"] valueForKey:@"token"] objectAtIndex:0];
              //JsonMsg = [[responseObject valueForKey:@"participants"]valueForKey:@"user_name"];
              //NSLog(@"%@/n ilosc: %d",JsonMsg,[JsonMsg count]);
              completion(YES);
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(@"Error: %@", error);
              err = [NSString stringWithFormat:@"%ld", (long)[error code]];
              completion(NO);
          }];
    
}

-(void)getJSONByAddress:(NSString *)addr WithToken:(NSString *)tkn Completion:(void (^)(BOOL))completion{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [manager setResponseSerializer:[AFJSONResponseSerializer serializer]];
    [manager POST:addr
       parameters:@{@"token":tkn}
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              JsonMsg = [[responseObject valueForKey:@"participants"] objectAtIndex:0];
              //JsonMsg = [[responseObject valueForKey:@"participants"]valueForKey:@"user_name"];
              NSLog(@"%@/n ilosc: %d",JsonMsg,[JsonMsg count]);
              completion(YES);
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(@"Errorr: %ld", (long)[error code]);
              err = [NSString stringWithFormat:@"%ld", (long)[error code]];
              completion(NO);
          }];

    
}

-(void)getJSONMeetingsByAddress:(NSString *)addr WithToken:(NSString *)tkn Completion:(void (^)(BOOL))completion{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [manager setResponseSerializer:[AFJSONResponseSerializer serializer]];
    [manager POST:addr
       parameters:@{@"token":tkn}
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              //NSLog(@"%@", responseObject);
              JsonMsg = [[responseObject objectAtIndex:0] valueForKey:@"meetings"];
              JsonMsgDetail = [[responseObject objectAtIndex:0] valueForKey:@"meetings"];
              meetingsCount = [JsonMsg count];
              //NSLog(@"daj mnie cos: %@", [self getUserIDAtIndex:0]);
              //JsonMsg = [[responseObject valueForKey:@"participants"]valueForKey:@"user_name"];
              NSLog(@"%@/n ilosc: %d",JsonMsgDetail,[JsonMsg count]);
              [self getInviteState:0];
              [self getTime:0];
              completion(YES);
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(@"Error: %@", error);
              err = [NSString stringWithFormat:@"%ld", (long)[error code]];
              completion(NO);
          }];
    
    
}

-(void)getJSONByAddress:(NSString *)addr WithToken:(NSString *)tkn UserID:(NSString *)uid Completion:(void (^)(BOOL))completion{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [manager setResponseSerializer:[AFJSONResponseSerializer serializer]];
    [manager POST:addr
       parameters:@{@"token":tkn,@"id":uid}
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              JsonMsgDetail = [[responseObject objectAtIndex:0] valueForKey:@"participant_details"];
              questionnaire =[[JsonMsgDetail objectAtIndex:0] valueForKey:@"questionnaire"];
              quCount= (int)[questionnaire count];
              NSLog(@"Dane: %@ ilosc: %d", JsonMsgDetail, (int)[JsonMsgDetail count]);
              NSLog(@"JSon: %@ ilosc:%d", questionnaire, (int)[questionnaire count]);
              if (quCount >= 2) {
                  
                  [self getDescriptionAtIndex:2];
              [self getValueArIndex:2];
              }
              if([questionnaire count])
                  NSLog(@"Get value for index: %@", [questionnaire objectAtIndex:0]);
              completion(YES);
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(@"Error: %@", error);
              err = [NSString stringWithFormat:@"%ld", (long)[error code]];
              completion(NO);
          }];
    
    
}

-(NSString*)getMeetingUserId:(int)index{
    NSString *uid;
    uid = [NSString stringWithFormat:@"%@",[[JsonMsg objectAtIndex:index ] valueForKey:@"user_id"]];
    NSLog(@"tu powinno byc id: %@",uid);
    return uid;
}

-(int)getMeetingId:(int)index{
    NSString *meet;
    meet = [NSString stringWithFormat:@"%@",[[JsonMsg objectAtIndex:index ] valueForKey:@"id"]];
    return [meet intValue];
}

-(NSString*)getInviteState:(int)index{
    NSString *is;
    is = [NSString stringWithFormat:@"%@",[[JsonMsg objectAtIndex:index ] valueForKey:@"invite_state"]];
    NSLog(@"sre: %@",is);
    return is;
}

-(NSString*)getUserNameAtIndex:(int)index{
    NSString *userName;
    userName = [NSString stringWithFormat:@"%@",[[JsonMsg objectAtIndex:index ] valueForKey:@"user_name"]];
    
    return userName;
}

-(NSDate*)getTime:(int)index{
    NSString *time;
    time = [NSString stringWithFormat:@"%@",[[JsonMsg objectAtIndex:index ] valueForKey:@"time"]];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"YYYY-MM-dd'T'HH:mm:ss'+1:00'"];
    [dateFormat setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    NSDate *dte = [dateFormat dateFromString:time];
    NSLog(@"Date: %@", dte);
    
    return dte;
}

-(NSString*)getCompanyAtIndex:(int)index{
    NSString *company;
    company = [NSString stringWithFormat:@"%@",[[JsonMsg objectAtIndex:index ] valueForKey:@"user_company"]];
    
    return company;
}

-(NSString*)getUserIDAtIndex:(int)index{
    NSString *uid;
    uid = [NSString stringWithFormat:@"%@",[[JsonMsg objectAtIndex:index ] valueForKey:@"user_id"]];
    NSLog(@"%@",uid);
    return uid;
}

-(NSString*)getAvatarURLAtIndex:(int)index{
    NSString *avatar;
    avatar = [NSString stringWithFormat:@"%@",[[JsonMsg objectAtIndex:index ] valueForKey:@"user_avatar"]];
    return avatar;
}

-(NSInteger* )getUserCount{
    return (NSInteger*)[JsonMsg count];
}



-(NSDate*)getFullDateAtIndex:(int)index{
  
    NSString *dataStr = [NSString stringWithFormat:@"%@",[[programs objectAtIndex:index] valueForKey:@"day"]];
    NSDateFormatter * dateFormatter= [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    [dateFormatter setDateFormat:@"rrrr-mm-dd"];
    fullDate=[dateFormatter dateFromString:dataStr];
    return fullDate;
}

-(NSString*)getDayHumanAtIndex:(int)index{
    
    dateTitle = [NSString stringWithFormat:@"%@",[[programs objectAtIndex:index] valueForKey:@"day_human"]];
    return dateTitle;
    
}

-(NSString*)getHourInDay:(int)day AtIndex:(int)index{
    NSString *hour = [NSString stringWithFormat:@"%@",[[[[programs objectAtIndex:day] valueForKey:@"day_program"] objectAtIndex:index] valueForKey:@"hours"]];
    return hour;
}

-(NSString* )getDetailValueInDay:(int)day AtIndex:(int)index{
    NSString *fvalue = [NSString stringWithFormat:@"%@",[[[[programs objectAtIndex:day] valueForKey:@"day_program"] objectAtIndex:index] valueForKey:@"value"]];
    return fvalue;
}

-(NSString *)getcompanyPosition{
    NSString *cp = [NSString stringWithFormat:@"%@",[[JsonMsgDetail valueForKey:@"user_comp_position"] objectAtIndex:0]];
    return cp;
}

-(NSString *)getcompany{
    NSString *cp = [NSString stringWithFormat:@"%@",[[JsonMsgDetail valueForKey:@"user_company"] objectAtIndex:0]];
    
    NSLog(@"company: %@", JsonMsgDetail);
    return cp;
}

-(NSString *)getMessage:(int)index{
    NSString *fvalue = [NSString stringWithFormat:@"%@",[[JsonMsg objectAtIndex:index ] valueForKey:@"message"]];
    NSLog(@"message: %@ %@", fvalue,JsonMsg);
    return fvalue;
}

-(NSString *)getUserEmail{
    NSString *cp = [NSString stringWithFormat:@"%@",[[JsonMsgDetail valueForKey:@"user_email"] objectAtIndex:0]];
    return cp;
}

-(NSString *)getUserPhone1{
    NSString *cp = [NSString stringWithFormat:@"%@",[[JsonMsgDetail valueForKey:@"user_phone1"] objectAtIndex:0]];
    return cp;
}

-(NSString *)getUserPhone2{
    NSString *cp = [NSString stringWithFormat:@"%@",[[JsonMsgDetail valueForKey:@"user_phone2"] objectAtIndex:0]];
    return cp;
}

-(NSString *)getUserName{
    NSString *cp = [NSString stringWithFormat:@"%@",[[JsonMsgDetail valueForKey:@"user_name"] objectAtIndex:0]];
    return cp;
}

-(NSString *)getUserCompany{
    NSString *cp = [NSString stringWithFormat:@"%@",[[JsonMsgDetail valueForKey:@"user_company"] objectAtIndex:0]];
    return cp;
}

-(NSString *)getUserAvatar{
    NSString *cp = [NSString stringWithFormat:@"%@",[[JsonMsgDetail valueForKey:@"user_avatar"] objectAtIndex:0]];
    return cp;
}

-(NSString *)getUserPaid{
    NSString *cp = [NSString stringWithFormat:@"%@",[[JsonMsgDetail valueForKey:@"user_paid"] objectAtIndex:0]];
    return cp;
}



-(NSString *)getDescriptionAtIndex:(int)index{
    
    NSString *description;
    description = [[questionnaire objectAtIndex:index] valueForKey:@"description"];
    
    NSLog(@"descccccc >>>> %@", description);
    
    return description;
}

-(NSArray *)getValueArIndex:(int)index{
    
    NSArray *tab = [[questionnaire valueForKey:@"value"] objectAtIndex:index];
    return tab;
    
}

-(NSString*)getToken{
    return token;
}

-(int)getDayProgramCount:(int)day{
    int DPC = [[[programs objectAtIndex:day] valueForKey:@"day_program"] count];
    return DPC;
}

-(int)getDayCount{
    return programListCount;
}

-(int)getQuestCount{
    return quCount;
}

-(int)getMeetingsCount{
    return meetingsCount;
}

-(int)getValueCount{
    return valCount;
}

-(NSString* )getError{
    
    return err;
}
     
@end
