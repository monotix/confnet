//
//  TabBarDelegate.h
//  Confnet
//
//  Created by Rafał Osowicki on 15.07.2015.
//  Copyright (c) 2015 Rafał Osowicki. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface TabBarDelegate : NSObject<UITabBarControllerDelegate>

@end
