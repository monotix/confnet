//
//  Participant.h
//  Confnet
//
//  Created by Rafał Osowicki on 03.08.2015.
//  Copyright (c) 2015 Rafał Osowicki. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class QuestionAnswer;

@interface Participant : NSManagedObject

@property (nonatomic, retain) NSString * avatarURL;
@property (nonatomic, retain) NSString * company;
@property (nonatomic, retain) NSString * company_position;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * phone1;
@property (nonatomic, retain) NSString * phone2;
@property (nonatomic, retain) NSString * user_name;
@property (nonatomic, retain) NSString * user_paid;
@property (nonatomic, retain) NSString * userID;
@property (nonatomic, retain) NSSet *user_id;
@end

@interface Participant (CoreDataGeneratedAccessors)

- (void)addUser_idObject:(QuestionAnswer *)value;
- (void)removeUser_idObject:(QuestionAnswer *)value;
- (void)addUser_id:(NSSet *)values;
- (void)removeUser_id:(NSSet *)values;

@end
