//
//  OfflineViewController.h
//  Confnet
//
//  Created by Rafał Osowicki on 28.07.2015.
//  Copyright (c) 2015 Rafał Osowicki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBManager.h"

@interface OfflineViewController : UIViewController{
    DBManager *DBM;
}

@property (weak, nonatomic) IBOutlet UILabel *WorkLB;
@property (weak, nonatomic) IBOutlet UILabel *RetryLB;
@property (weak, nonatomic) IBOutlet UILabel *LogoutLB;

@end
