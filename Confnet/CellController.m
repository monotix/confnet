//
//  CellController.m
//  Confnet
//
//  Created by Rafał Osowicki on 03.07.2015.
//  Copyright (c) 2015 Rafał Osowicki. All rights reserved.
//

#import "CellController.h"
#import "DetailViewController.h"

@implementation CellController{
    
    UILabel *_nameUserLB;
    UILabel *_companyLB;
    UIImageView *_avatarIMG;
    
    UILabel *_titleDateLB;
    UILabel *_hoursLB;
    UILabel *_valueLB;
    
    UILabel *_timeLB;
    UILabel *_statusLB;
    UILabel *_nameLB;
    UILabel *_companyMeetingLB;
    UIButton *userDetailBT;
    UIButton *showMessage;
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
 
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    _screenBound = [[UIScreen mainScreen] bounds];
    
    if(self){
        
        if ([reuseIdentifier isEqualToString:@"ProgramCell"]) {

            CGRect titleValueRectangle = CGRectMake(_screenBound.size.width*0.10, 24, 200, 30);
            _titleDateLB = [[UILabel alloc] initWithFrame:titleValueRectangle];
            //_titleDateLB.font = [UIFont boldSystemFontOfSize:25];
            _titleDateLB.font = [UIFont systemFontOfSize:18];
            //_titleDateLB.textAlignment = NSTextAlignmentCenter;
            //_titleDateLB.layer.borderColor = [UIColor greenColor].CGColor;
            //_titleDateLB.layer.borderWidth = 3.0;
            _titleDateLB.textColor = [UIColor colorWithRed:79.0f/255.0f green:31.0f/255.0f blue:172.0f/255.0f alpha:1.0];
            [self.contentView addSubview: _titleDateLB];
            
        }else if([reuseIdentifier isEqualToString:@"ParticipantsCell"]){
            
            CGRect userNameRectangle = CGRectMake(_screenBound.size.width*0.25, 5, _screenBound.size.width*0.9- _screenBound.size.width*0.2-10, 30);
            _nameUserLB = [[UILabel alloc] initWithFrame:userNameRectangle];
            //_nameUserLB.font = [UIFont boldSystemFontOfSize:25];
            _nameUserLB.font = [UIFont systemFontOfSize:15];
            _nameUserLB.textColor = [UIColor colorWithRed:79.0f/255.0f green:31.0f/255.0f blue:172.0f/255.0f alpha:1.0];
            [self.contentView addSubview: _nameUserLB];
            
            CGRect companyRectangle = CGRectMake(_screenBound.size.width*0.25, 25, _screenBound.size.width*0.9- _screenBound.size.width*0.2-10, 30);
            _companyLB = [[UILabel alloc] initWithFrame:companyRectangle];
            //_nameUserLB.font = [UIFont boldSystemFontOfSize:25];
            _companyLB.font = [UIFont systemFontOfSize:12];
            _companyLB.textColor = [UIColor grayColor];
            [self.contentView addSubview: _companyLB];
            
            _avatarIMG = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 60, 60)];
            [self.contentView addSubview: _avatarIMG];
            
        }else if ([reuseIdentifier isEqualToString:@"MeetingsCell"]){
            
            CGRect dateValueRectangle = CGRectMake(_screenBound.size.width*0.05, _screenBound.size.height*0.01, 130, 30);
            _timeLB = [[UILabel alloc] initWithFrame:dateValueRectangle];
            _timeLB.font = [UIFont systemFontOfSize:14];
            _timeLB.textColor = [UIColor colorWithRed:79.0f/255.0f green:31.0f/255.0f blue:172.0f/255.0f alpha:1.0];
            [self.contentView addSubview: _timeLB];
            
            CGRect statusValueRectangle = CGRectMake(_screenBound.size.width*0.45, _screenBound.size.height*0.01, 100, 30);
            _statusLB = [[UILabel alloc] initWithFrame:statusValueRectangle];
            _statusLB.font = [UIFont systemFontOfSize:14];
            _statusLB.textColor = [UIColor grayColor];
            [self.contentView addSubview: _statusLB];
            
            CGRect userNameRectangle = CGRectMake(_screenBound.size.width*0.05, _screenBound.size.height*0.09, _screenBound.size.width*0.9- _screenBound.size.width*0.2-10, 30);
            _nameUserLB = [[UILabel alloc] initWithFrame:userNameRectangle];
            _nameUserLB.font = [UIFont systemFontOfSize:15];
            _nameUserLB.textColor = [UIColor blackColor];
            [self.contentView addSubview: _nameUserLB];
            
            CGRect compNameRectangle = CGRectMake(_screenBound.size.width*0.05, _screenBound.size.height*0.09+20, _screenBound.size.width*0.9- _screenBound.size.width*0.2-10, 30);
            _companyLB = [[UILabel alloc] initWithFrame:compNameRectangle];
            _companyLB.font = [UIFont systemFontOfSize:15];
            _companyLB.textColor = [UIColor grayColor];
            [self.contentView addSubview: _companyLB];
            
            _avatarIMG = [[UIImageView alloc] initWithFrame:CGRectMake(_screenBound.size.width*0.70, _screenBound.size.height*0.03, 60, 60)];
            _avatarIMG.layer.cornerRadius = _avatarIMG.frame.size.width / 2;
            _avatarIMG.clipsToBounds = YES;
            [self.contentView addSubview: _avatarIMG];
            
            
        }
        
    }
    
    return self;
}

-(void)setSelected:(BOOL)selected animated:(BOOL)animated{
    [super setSelected:selected animated:animated];
}



//////////////////Program Cells////////////////////////

-(void)setTitleDate:(NSString *)titleDate{
    
    if(![titleDate isEqualToString: _titleDate]){
        _titleDate = [titleDate copy];
        _titleDateLB.text = _titleDate;
    }
}

-(void)setValue:(NSMutableArray *)value{
    if(![value isEqualToArray:_value]){
        _value = [value copy];
        
        float gap = 30.0;
        
        
            for(int i=0; i< [_value count]; i++)
                {
                    
                    if(gap != 30){
                        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(_screenBound.size.width*0.05, 30+gap, _screenBound.size.width*0.82, 0.5f)];
                        line.backgroundColor = [UIColor colorWithRed:(200.0f/255.0f) green:(200.0f/255.0f) blue:(200.0f/255.0f) alpha:1.0f];
                        line.layer.masksToBounds =NO;
                        [self.contentView addSubview:line];
                    }
            
                    CGRect titleValueRectangle = CGRectMake(_screenBound.size.width*0.35, 30+gap, 190, 30);
            _valueLB = [[UILabel alloc] initWithFrame:titleValueRectangle];
            _valueLB.font = [UIFont boldSystemFontOfSize:25];
            _valueLB.font = [UIFont systemFontOfSize:12];
            _valueLB.lineBreakMode = NSLineBreakByWordWrapping;
            _valueLB.numberOfLines = 0;
            _valueLB.tag = i;
            [self.contentView addSubview: _valueLB];
            
            _valueLB.text = [_value objectAtIndex:i];
            
            gap+=33.0;
                    
                    //[self setFrame:CGRectMake(self.bounds.origin.x, self.bounds.origin.y, self.bounds.size.width, 1000)];
                   
        }
    }
}

-(void) setHours:(NSMutableArray *)hours{
    if(![hours isEqualToArray: _hours]){
        _hours = [hours copy];
        
        float gap = 30.0;
        
        for(int i=0; i< [_value count]; i++)
        {
            CGRect titleValueRectangle = CGRectMake(_screenBound.size.width*0.05, 30+gap, 100, 20);
            _hoursLB = [[UILabel alloc] initWithFrame:titleValueRectangle];
            _hoursLB.font = [UIFont boldSystemFontOfSize:25];
            _hoursLB.font = [UIFont systemFontOfSize:12];
            _hoursLB.tag = i;
            [self.contentView addSubview: _hoursLB];
            
            if(![[_hours objectAtIndex:i] isEqualToString:@"(null)"])
                _hoursLB.text = [_hours objectAtIndex:i];
            
            gap+=35.0;
            
        }
    }
}

-(void)setUserName:(NSString *)userName
{
    if(![userName isEqualToString: _userName]){
        _userName = [userName copy];
        _nameUserLB.text = _userName;
    }
}

-(void)setCompany:(NSString *)c
{
    if(![c isEqualToString: _userName]){
        _company = [c copy];
        
        if(![_company isEqualToString:@"<null>"])
            _companyLB.text = _company;
    }
}

-(void)setAvatarUrl:(NSString *)avatarUrl
{
    if(![avatarUrl isEqualToString: _avatarUrl]){
        _avatarUrl = [avatarUrl copy];
        _avatarIMG=[[UIImageView alloc]init];
        NSURL *url = [NSURL URLWithString:avatarUrl];
        NSData *data = [NSData dataWithContentsOfURL : url];
        UIImage *image = [UIImage imageWithData: data];
        _avatarIMG.image = image;
    }
}

-(void)setFrame:(CGRect)frame {
    frame.origin.x += 10;
    frame.size.width = _screenBound.size.width - 20;
    [super setFrame:frame];
}

-(void)setTime:(NSDate *)time{
    
    if(![time isEqualToDate: _time]){
        _time = [time copy];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
        NSLog(@"data: %@",_time);
        [dateFormatter setDateFormat:@"yyyy.MM.dd kk:mm "];
        NSString *dateText = [dateFormatter stringFromDate:_time];
        _timeLB.text = dateText;
    }
    
}

-(void)setStatus:(NSString *)status{
    
    if(![status isEqualToString:_status]){
        _status = [status copy];
         NSLog(@"celll %@", _status);
        _statusLB.text = _status;
    }
}


@end


