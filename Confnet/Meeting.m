//
//  Meeting.m
//  Confnet
//
//  Created by Rafał Osowicki on 13.07.2015.
//  Copyright (c) 2015 Rafał Osowicki. All rights reserved.
//

#import "Meeting.h"

@implementation Meeting
@synthesize inviteState,mid,time,msg,usrID;

-(id)initWithMid:(int)m InviteState:(NSString*)is Time:(NSDate*)t Message:(NSString*)msgs UserID:(NSString *)ui{
    
    if(self){
        self.mid = m;
        self.inviteState = is;
        self.time = t;
        self.msg = msgs;
        self.usrID = ui;
    }
    return self;
}

-(id)initWithMeeting:(Meeting *)src{
    
    if(self){
        self.mid = src.mid;
        self.inviteState = src.inviteState;
        self.time = src.time;
        self.msg = src.msg;
        self.usrID = src.usrID;
    }
    
    return self;
}

@end
