//
//  OfflineViewController.m
//  Confnet
//
//  Created by Rafał Osowicki on 28.07.2015.
//  Copyright (c) 2015 Rafał Osowicki. All rights reserved.
//

#import "OfflineViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "LogInViewController.h"

@interface OfflineViewController ()

@end

@implementation OfflineViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _WorkLB.layer.shadowOffset = CGSizeMake(1, 0);
    _WorkLB.layer.shadowColor = [[UIColor blackColor] CGColor];
    _WorkLB.layer.shadowRadius = 5;
    _WorkLB.layer.shadowOpacity = 0.5;
    
    UITapGestureRecognizer *tapGestureOffline =
    [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(offline:)];
    [_WorkLB addGestureRecognizer:tapGestureOffline];
    
    _RetryLB.layer.shadowOffset = CGSizeMake(1, 0);
    _RetryLB.layer.shadowColor = [[UIColor blackColor] CGColor];
    _RetryLB.layer.shadowRadius = 5;
    _RetryLB.layer.shadowOpacity = 0.5;
    
    UITapGestureRecognizer *tapGestureRetry =
    [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(retry:)];
    [_RetryLB addGestureRecognizer:tapGestureRetry];
    
    _LogoutLB.layer.shadowOffset = CGSizeMake(1, 0);
    _LogoutLB.layer.shadowColor = [[UIColor blackColor] CGColor];
    _LogoutLB.layer.shadowRadius = 5;
    _LogoutLB.layer.shadowOpacity = 0.5;
    
    UITapGestureRecognizer *tapGestureLogout =
    [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(logout:)];
    [_LogoutLB addGestureRecognizer:tapGestureLogout];
    
    DBM = [DBManager sharedManager];
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

-(IBAction)offline:(id)sender{
    [DBM setinternetConnection:YES];
    [self performSegueWithIdentifier:@"offline" sender:self];
}

-(IBAction)retry:(id)sender{
    
    [self performSegueWithIdentifier:@"toLogin" sender:self];
}

-(IBAction)logout:(id)sender{
    
    [DBM deleteToken];
    [DBM deleteProgramFromDB];
    [DBM deleteParticipanFromDB];
    [DBM deleteMeetingsFromDB];
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logOut];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    LogInViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"login"];
    [topController presentViewController:vc animated:YES completion:nil];
    
}



@end
