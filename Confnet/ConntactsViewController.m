//
//  ConntactsViewController.m
//  Confnet
//
//  Created by Rafał Osowicki on 02.07.2015.
//  Copyright (c) 2015 Rafał Osowicki. All rights reserved.
//

#import "ConntactsViewController.h"

@interface ConntactsViewController (){
    UILabel *contentLB;
    float gap;
    NSString *name;
    NSString *company;
}

@end

@implementation ConntactsViewController

- (void)loadView{
    
    CGRect fullScreenRect=[[UIScreen mainScreen] applicationFrame];
    scrollView=[[UIScrollView alloc] initWithFrame:fullScreenRect];
    scrollView.contentSize=CGSizeMake(320,650);
    
    self.view=scrollView;
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIImage* image3 = [UIImage imageNamed:@"ic_action_logout.png"];
    CGRect frameimg = CGRectMake(15,5, 30,30);
    UIButton *logoutButton = [[UIButton alloc] initWithFrame:frameimg];
    [logoutButton setBackgroundImage:image3 forState:UIControlStateNormal];
    [logoutButton addTarget:self action:@selector(Logout)
           forControlEvents:UIControlEventTouchUpInside];
    [logoutButton setShowsTouchWhenHighlighted:YES];
    
    UIBarButtonItem *logoutButton1 =[[UIBarButtonItem alloc] initWithCustomView:logoutButton];
    
    UINavigationItem *navItem = [[UINavigationItem alloc] init];
    navItem.rightBarButtonItem = logoutButton1;
    navItem.title = @"Contact";
    super.bar.items = @[ navItem ];
    self.view.backgroundColor = [UIColor colorWithRed:0.93 green:0.93 blue:0.93 alpha:1.0];
    
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeLeft:)];
    swipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipeLeft];
    swipeLeft.delegate = self;
    
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRight:)];
    swipeRight.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swipeRight];
    swipeRight.delegate = self;
    
    _screenBound = [[UIScreen mainScreen] bounds];
    
    for(gap = 0; gap < 420; gap+=70){
        
        switch ((int)gap)
        
        {
            case 0:
                name = [NSString stringWithFormat:@"    Rafał Wójcicki"];
                company = [NSString stringWithFormat:@"    Managing director"];
                break;
                
            case 70:
                name = [NSString stringWithFormat:@"    Karolina Gawron"];
                company = [NSString stringWithFormat:@"    Event-specialist"];
                break;
                
            case 140:
                name = [NSString stringWithFormat:@"    Paulina Maciboch"];
                company = [NSString stringWithFormat:@"    Event-specialist"];
                break;
                
            case 210:
                name = [NSString stringWithFormat:@"    Zuzanna Pajorska"];
                company = [NSString stringWithFormat:@"    Event-specialist"];
                break;
                
            case 280:
                name = [NSString stringWithFormat:@"    Barbara Didouh"];
                company = [NSString stringWithFormat:@"     Event-specialist"];
                break;
                
            case 350:
                name = [NSString stringWithFormat:@"    Iryna Gorbunova"];
                company = [NSString stringWithFormat:@"     Event-specialist"];
                break;
                
        }
        
        CGRect contetntRectangle = CGRectMake(_screenBound.size.width*0.02, _screenBound.size.height*0.15+gap, _screenBound.size.width*0.98 - _screenBound.size.width*0.02, 60);
        contentLB = [[UILabel alloc] initWithFrame:contetntRectangle];
        contentLB.font = [UIFont systemFontOfSize:12];
        contentLB.textColor = [UIColor grayColor];
        contentLB.layer.shadowOffset = CGSizeMake(1, 0);
        contentLB.layer.shadowColor = [[UIColor blackColor] CGColor];
        contentLB.layer.shadowRadius = 2;
        contentLB.layer.shadowOpacity = .25;
        contentLB.layer.backgroundColor = [UIColor whiteColor].CGColor;
        [self.view addSubview: contentLB];
        
        CGRect cBounds = contetntRectangle;
        //NSLog(@"x: %f, y: %f, width: %f, height: %f",cBounds.origin.x,cBounds.origin,cBounds.size.width*0.5,cBounds.size.height);
        CGRect userNameRectangle = CGRectMake(cBounds.origin.x, cBounds.origin.y+5, cBounds.size.width*0.7, 25);
        UILabel *UserNameLB = [[UILabel alloc] initWithFrame:userNameRectangle];
        //_nameUserLB.font = [UIFont boldSystemFontOfSize:25];
        UserNameLB.font = [UIFont systemFontOfSize:16];
        UserNameLB.textColor = [UIColor colorWithRed:79.0f/255.0f green:31.0f/255.0f blue:172.0f/255.0f alpha:1.0];
        UserNameLB.layer.backgroundColor = [UIColor clearColor].CGColor;
        UserNameLB.text = name;
        [self.view addSubview: UserNameLB];
        
        //NSLog(@"x: %f, y: %f, width: %f, height: %f",cBounds.origin.x,cBounds.origin,cBounds.size.width*0.5,cBounds.size.height);
        CGRect userCompanyRectangle = CGRectMake(cBounds.origin.x, cBounds.origin.y+25, cBounds.size.width*0.7, 25);
        UILabel *UserCompanyLB = [[UILabel alloc] initWithFrame:userCompanyRectangle];
        UserCompanyLB.font = [UIFont systemFontOfSize:14];
        UserCompanyLB.textColor = [UIColor grayColor];
        UserCompanyLB.text = company;
        [self.view addSubview: UserCompanyLB];
        
        CGRect phoneRectangle = CGRectMake(cBounds.origin.x+cBounds.size.width-100, cBounds.origin.y, 50, 50);
        UILabel *phoneLB = [[UILabel alloc] initWithFrame:phoneRectangle];
        phoneLB.textColor = [UIColor grayColor];
        [phoneLB setTag:gap];
        
        phoneLB.userInteractionEnabled = YES;
        UITapGestureRecognizer *tapGesture =
        [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(callPhone:)];
        [phoneLB addGestureRecognizer:tapGesture];
        
        [self.view addSubview: phoneLB];
        
        UIImageView *phoneIMG =[[UIImageView alloc] initWithFrame:CGRectMake(cBounds.origin.x+cBounds.size.width-83,cBounds.origin.y+20,18,18)];
        phoneIMG.image=[UIImage imageNamed:@"phone2.png"];
        [self.view addSubview:phoneIMG];
        
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(cBounds.origin.x+cBounds.size.width-50, cBounds.origin.y, 1, 60)];
        line.backgroundColor = [UIColor colorWithRed:(200.0f/255.0f) green:(200.0f/255.0f) blue:(200.0f/255.0f) alpha:1.0f];
    
        [self.view addSubview:line];
        
        CGRect emailRectangle = CGRectMake(cBounds.origin.x+cBounds.size.width-50, cBounds.origin.y, 50, 50);
        UILabel *emailLB = [[UILabel alloc] initWithFrame:emailRectangle];
        //_nameUserLB.font = [UIFont boldSystemFontOfSize:25];
        emailLB.font = [UIFont systemFontOfSize:14];
        emailLB.textColor = [UIColor grayColor];
        
        [emailLB setTag:gap];
        
        emailLB.userInteractionEnabled = YES;
        UITapGestureRecognizer *tapGesturee =
        [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sendEmail:)];
        [emailLB addGestureRecognizer:tapGesturee];
        [self.view addSubview: emailLB];
        
        UIImageView *emailIMG =[[UIImageView alloc] initWithFrame:CGRectMake(cBounds.origin.x+cBounds.size.width-33,cBounds.origin.y+20,18,18)];
        emailIMG.image=[UIImage imageNamed:@"email.png"];
        [self.view addSubview:emailIMG];
        
        
        UIView *line1 = [[UIView alloc] initWithFrame:CGRectMake(cBounds.origin.x+cBounds.size.width-100, cBounds.origin.y, 1, 60)];
        line1.backgroundColor = [UIColor colorWithRed:(200.0f/255.0f) green:(200.0f/255.0f) blue:(200.0f/255.0f) alpha:1.0f];
        line1.layer.masksToBounds =NO;
        [self.view addSubview:line1];
        
    }
    
    CGRect contetntRectangle = CGRectMake(_screenBound.size.width*0.02, _screenBound.size.height*0.15+440, _screenBound.size.width*0.98- _screenBound.size.width*0.02, 75);
    contentLB = [[UILabel alloc] initWithFrame:contetntRectangle];
    //_nameUserLB.font = [UIFont boldSystemFontOfSize:25];
    contentLB.font = [UIFont systemFontOfSize:12];
    contentLB.textColor = [UIColor grayColor];
    //TopLB.layer.borderColor = [UIColor blackColor].CGColor;
    //TopLB.layer.borderWidth = 0.5;
    contentLB.layer.shadowOffset = CGSizeMake(1, 0);
    contentLB.layer.shadowColor = [[UIColor blackColor] CGColor];
    contentLB.layer.shadowRadius = 2;
    contentLB.layer.shadowOpacity = .25;
    contentLB.layer.backgroundColor = [UIColor whiteColor].CGColor;
    [self.view addSubview: contentLB];
    
    CGRect cBounds = contetntRectangle;
    //NSLog(@"x: %f, y: %f, width: %f, height: %f",cBounds.origin.x,cBounds.origin,cBounds.size.width*0.5,cBounds.size.height);
    CGRect userNameRectangle = CGRectMake(cBounds.origin.x, cBounds.origin.y, cBounds.size.width*0.7, 25);
    UILabel *UserNameLB = [[UILabel alloc] initWithFrame:userNameRectangle];
    //_nameUserLB.font = [UIFont boldSystemFontOfSize:25];
    UserNameLB.font = [UIFont systemFontOfSize:16];
    UserNameLB.textColor = [UIColor colorWithRed:79.0f/255.0f green:31.0f/255.0f blue:172.0f/255.0f alpha:1.0];
    UserNameLB.layer.backgroundColor = [UIColor clearColor].CGColor;
    UserNameLB.text = @"    Office";
    [self.view addSubview: UserNameLB];
    
    //NSLog(@"x: %f, y: %f, width: %f, height: %f",cBounds.origin.x,cBounds.origin,cBounds.size.width*0.5,cBounds.size.height);
    CGRect userCompanyRectangle = CGRectMake(cBounds.origin.x, cBounds.origin.y+20, cBounds.size.width*0.7, 25);
    UILabel *UserCompanyLB = [[UILabel alloc] initWithFrame:userCompanyRectangle];
    //_nameUserLB.font = [UIFont boldSystemFontOfSize:25];
    UserCompanyLB.font = [UIFont systemFontOfSize:14];
    UserCompanyLB.textColor = [UIColor grayColor];
    UserCompanyLB.layer.backgroundColor = [UIColor clearColor].CGColor;
    UserCompanyLB.text = @"     Pl. Grunwaldzki 16/46";
    [self.view addSubview: UserCompanyLB];
    
    CGRect userCompanyAddrRectangle = CGRectMake(cBounds.origin.x, cBounds.origin.y+40, cBounds.size.width*0.7, 25);
    UILabel *UserCompanyAddrLB = [[UILabel alloc] initWithFrame:userCompanyAddrRectangle];
    //_nameUserLB.font = [UIFont boldSystemFontOfSize:25];
    UserCompanyAddrLB.font = [UIFont systemFontOfSize:14];
    UserCompanyAddrLB.textColor = [UIColor grayColor];
    UserCompanyAddrLB.layer.backgroundColor = [UIColor clearColor].CGColor;
    UserCompanyAddrLB.text = @"     50-384 Wrocław, Poland";
    [self.view addSubview: UserCompanyAddrLB];
    
    CGRect phoneRectangle = CGRectMake(cBounds.origin.x+cBounds.size.width-100, cBounds.origin.y, 50, 75);
    UILabel *phoneLB = [[UILabel alloc] initWithFrame:phoneRectangle];
    //_nameUserLB.font = [UIFont boldSystemFontOfSize:25];
    phoneLB.font = [UIFont systemFontOfSize:14];
    phoneLB.textColor = [UIColor grayColor];
    [phoneLB setTag:360];
    
    phoneLB.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapGesture =
    [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(callPhone:)];
    [phoneLB addGestureRecognizer:tapGesture];
    [self.view addSubview: phoneLB];
    
    UIImageView *phoneIMG =[[UIImageView alloc] initWithFrame:CGRectMake(cBounds.origin.x+cBounds.size.width-85,cBounds.origin.y+25,18,18)];
    phoneIMG.image=[UIImage imageNamed:@"phone2.png"];
    [self.view addSubview:phoneIMG];
    
    
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(cBounds.origin.x+cBounds.size.width-50, cBounds.origin.y, 1, 75)];
    line.backgroundColor = [UIColor colorWithRed:(200.0f/255.0f) green:(200.0f/255.0f) blue:(200.0f/255.0f) alpha:1.0f];
    [self.view addSubview:line];
    
    CGRect emailRectangle = CGRectMake(cBounds.origin.x+cBounds.size.width-50, cBounds.origin.y, 50, 75);
    UILabel *emailLB = [[UILabel alloc] initWithFrame:emailRectangle];
    //_nameUserLB.font = [UIFont boldSystemFontOfSize:25];
    emailLB.font = [UIFont systemFontOfSize:14];
    [emailLB setTag:360];
    
    emailLB.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapGesturee =
    [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sendEmail:)];
    [emailLB addGestureRecognizer:tapGesturee];
    [self.view addSubview: emailLB];
    
    UIImageView *emailIMG =[[UIImageView alloc] initWithFrame:CGRectMake(cBounds.origin.x+cBounds.size.width-33,cBounds.origin.y+25,18,18)];
    emailIMG.image=[UIImage imageNamed:@"email.png"];
    [self.view addSubview:emailIMG];
    
    
    UIView *line1 = [[UIView alloc] initWithFrame:CGRectMake(cBounds.origin.x+cBounds.size.width-100, cBounds.origin.y, 1, 75)];
    line1.backgroundColor = [UIColor colorWithRed:(200.0f/255.0f) green:(200.0f/255.0f) blue:(200.0f/255.0f) alpha:1.0f];
    line1.layer.masksToBounds = NO;
    [self.view addSubview:line1];

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)callPhone:(id)sender{
    
    
    UITapGestureRecognizer *tapRecognizer = (UITapGestureRecognizer *)sender;
    NSString *number;
    switch ([tapRecognizer.view tag]) {
        case 0:
            number = [NSString stringWithFormat:@"telprompt:+48667740910"];
            break;
        case 70:
            number = [NSString stringWithFormat:@"telprompt:+48739503252"];
            break;
        case 140:
            number = [NSString stringWithFormat:@"telprompt:+48727875359"];
            break;
        case 210:
            number = [NSString stringWithFormat:@"telprompt:+48726503230"];
            break;
        case 280:
            number = [NSString stringWithFormat:@"telprompt:+48739459937"];
            break;
        case 350:
            number = [NSString stringWithFormat:@"telprompt:+48729682915"];
            break;
            
        default:
            number = [NSString stringWithFormat:@"telprompt:+48739503252"];
            break;
    }
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",number]]];
    NSLog (@"%d",[tapRecognizer.view tag]);
    
}

-(void)sendEmail:(id)sender{
    
    if (![DBMa hasConnectivity]) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Network" message: @"No internet connection." delegate: self
                                             cancelButtonTitle:@"OK"
                                             otherButtonTitles:nil,nil];
        
        [alert show];
        return;
    }
    
    UITapGestureRecognizer *tapRecognizer = (UITapGestureRecognizer *)sender;
    NSString *email;
    switch ([tapRecognizer.view tag]) {
        case 0:
            email = [NSString stringWithFormat:@"rwojcicki@polanditmeeting.com"];
            break;
        case 70:
            email = [NSString stringWithFormat:@"kgawron@polanditmeeting.com"];
            break;
        case 140:
            email = [NSString stringWithFormat:@"pmaciboch@polanditmeeting.com"];
            break;
        case 210:
            email = [NSString stringWithFormat:@"zpajorska@polanditmeeting.com"];
            break;
        case 280:
            email = [NSString stringWithFormat:@"bdidouch@polanditmeeting.com"];
            break;
        case 350:
            email = [NSString stringWithFormat:@"igorbunova@polanditmeeting.com"];
            break;
            
        default:
            email = [NSString stringWithFormat:@"meeting@polanditmeeting.com"];
            break;
    }
    
    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
    if (mailClass != nil) {
        
    MFMailComposeViewController *mailcontroller = [[MFMailComposeViewController alloc] init];
    mailcontroller.mailComposeDelegate = self;
    [mailcontroller setMailComposeDelegate:self];
    NSArray *emailArray = [[NSArray alloc] initWithObjects:email, nil];
    [mailcontroller setToRecipients:emailArray];
    //[self presentViewController:mailcontroller animated:YES completion:nil];
    
        if([mailClass canSendMail]) {
            [self presentModalViewController:mailcontroller animated:YES];
        }
    }
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    NSString *alertMsg;
    
    switch (result) {
        case MFMailComposeResultSent:
            NSLog(@"You sent the email.");
            alertMsg = [NSString stringWithFormat:@"You sent the email."];
            break;
        case MFMailComposeResultSaved:
            NSLog(@"You saved a draft of this email");
            alertMsg = [NSString stringWithFormat:@"You saved a draft of this email"];
            break;
        case MFMailComposeResultCancelled:
            NSLog(@"You cancelled sending this email.");
            alertMsg = [NSString stringWithFormat:@"You cancelled sending this email."];
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed:  An error occurred when trying to compose this email");
            alertMsg = [NSString stringWithFormat:@"Mail failed:  An error occurred when trying to compose this email"];
            break;
        default:
            NSLog(@"An error occurred when trying to compose this email");
            alertMsg = [NSString stringWithFormat:@"An error occurred when trying to compose this email"];
            break;
    }
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Email" message: alertMsg delegate: self
                                         cancelButtonTitle:@"OK"
                                         otherButtonTitles:nil,nil];
    
    [alert show];
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}

-(void) swipeRight:(UISwipeGestureRecognizer *) recognizer {
    if (recognizer.direction == UISwipeGestureRecognizerDirectionRight)
        [self.tabBarController setSelectedIndex:[self.tabBarController selectedIndex] - 1];
}

-(void) swipeLeft:(UISwipeGestureRecognizer *) recognizer {
    if (recognizer.direction == UISwipeGestureRecognizerDirectionLeft)
        [self.tabBarController setSelectedIndex:[self.tabBarController selectedIndex] + 1];
    
}

@end
