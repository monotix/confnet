//
//  LogInViewController.m
//  Confnet
//
//  Created by Rafał Osowicki on 02.07.2015.
//  Copyright (c) 2015 Rafał Osowicki. All rights reserved.
//

#import "LogInViewController.h"
#import <AFNetworking/AFNetworking.h>
#import "JsonManager.h"
#import "ParticipantsViewController.h"
#import "AppDelegate.h"
#import <CoreData/CoreData.h>
#import <Social/Social.h>


@implementation LogInViewController



-(void)viewWillAppear:(BOOL)animated{
    if ([self isNullOrEmpty:[DBM loadTokenFromDB]]) {
        [self performSegueWithIdentifier:@"show" sender:self];
    }
}
- (void)viewDidLoad {
    
    
    
    
    JManager = [[JsonManager alloc] init];
    DBM = [DBManager sharedManager];
    
    _LoginLB.layer.shadowOffset = CGSizeMake(1, 0);
    _LoginLB.layer.shadowColor = [[UIColor blackColor] CGColor];
    _LoginLB.layer.shadowRadius = 5;
    _LoginLB.layer.shadowOpacity = 0.5;
    
    _LoginFBLB.layer.shadowOffset = CGSizeMake(1, 0);
    _LoginFBLB.layer.shadowColor = [[UIColor blackColor] CGColor];
    _LoginFBLB.layer.shadowRadius = 5;
    _LoginFBLB.layer.shadowOpacity = 0.5;
    
    _LoginLinLB.layer.shadowOffset = CGSizeMake(1, 0);
    _LoginLinLB.layer.shadowColor = [[UIColor blackColor] CGColor];
    _LoginLinLB.layer.shadowRadius = 5;
    _LoginLinLB.layer.shadowOpacity = 0.5;
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    NSMutableArray *tab = [[NSMutableArray alloc] init];
    [tab addObject:@"sddasd"];
    [tab addObject:@"sddasd"];
    [tab addObject:@"sddasd"];
    [dict setObject:@"sdas" forKey:@"kpka"];
    [dict setObject:@"sdasdasds" forKey:@"kpka"];
    [dict setObject:tab forKey:@"ksdsaa"];
    
    
    NSLog(@"%@",dict);
    
    [DBM setinternetConnection:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



- (IBAction)LogIn:(id)sender {
    
    if (![DBM hasConnectivity]) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Network" message: @"No internet connection." delegate: self
                                             cancelButtonTitle:@"OK"
                                             otherButtonTitles:nil,nil];
        
        [alert show];
        
        return;
    }

    NSString *login = _LoginTF.text;
    NSString *password = _PasswordTF.text;
    if([self isNullOrEmpty:login] || [self isNullOrEmpty:password])
    {
        UIAlertView *alertAddress = [[UIAlertView alloc]initWithTitle: @"Error" message: @"Fields can not be empty" delegate: self
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles:nil,nil];
        
        [alertAddress show];
        return;
    }
    
    //NSString *address = [NSString stringWithFormat:@"http://10.0.1.15/wp-json/cn/auth/wordpress"];
    NSString *address = [NSString stringWithFormat:@"http://polanditmeeting.com/wp-json/cn/auth/wordpress"];
    [JManager getJSONByAddress:address Login:login Password:password Completion:^(BOOL success) {
    
        if(!success){
            UIAlertView *alertAddress = [[UIAlertView alloc]initWithTitle: @"Login failed" message: @"Invalid email or password. \n Connection lost." delegate: self
                                                        cancelButtonTitle:@"OK"
                                                        otherButtonTitles:nil,nil];
            
            [alertAddress show];
            //return;
        }else{
            NSLog(@"token: %@", [JManager getToken]);
            [DBM saveTokenToDB:[JManager getToken]];
            NSLog(@" zaladowany: %@",[DBM loadTokenFromDB]);
        [self performSegueWithIdentifier:@"show" sender:self];
        }
    }];

    
}
- (IBAction)LogInFB:(id)sender {
    
    if (![DBM hasConnectivity]) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Network" message: @"No internet connection." delegate: self
                                             cancelButtonTitle:@"OK"
                                             otherButtonTitles:nil,nil];
        
        [alert show];
        return;
    }
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logOut];
    [login logInWithReadPermissions:@[@"public_profile"] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error) {
            NSLog(@"Error :%@", error);
        } else if (result.isCancelled) {
            NSLog(@"Error1 :%@", error);
        } else {
            
            if ([result.grantedPermissions containsObject:@"public_profile"]) {
                NSLog(@"token? : %@", [[FBSDKAccessToken currentAccessToken] tokenString]);
                
                [JManager getJSONByAddress:@"http://polanditmeeting.com/wp-json/cn/auth/facebook" WithFacebookToken:[[FBSDKAccessToken currentAccessToken] tokenString] Completion:^(BOOL success) {
                    
                    if(success){
                        
                        [DBM saveTokenToDB:[JManager getToken]];
                        NSLog(@" zaladowany: %@",[DBM loadTokenFromDB]);
                        [self performSegueWithIdentifier:@"show" sender:self];
                    
                    }else{
                        
                    }
                    
                }];
                
                

            }
        }
    }];

}

- (IBAction)LogInLinkedin:(id)sender {
    
    [self performSegueWithIdentifier:@"web" sender:self];
}


-(BOOL)isNullOrEmpty:(NSString *)x {
    BOOL retVal = YES;
    
    if(x != nil)
    {
        if( [x isKindOfClass:[NSString class]] )
        {
            retVal = x.length == 0;
        }
        else
        {
            NSLog(@"isNullOrEmpty, value not a string");
        }
    }
    return retVal;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [[self view] endEditing:YES];
}

-(void)viewDidAppear:(BOOL)animated{
    NSLog(@"pusty czy nie?  %@ %d %d",[DBM loadTokenFromDB],[DBM hasConnectivity], [self isNullOrEmpty:[DBM loadTokenFromDB]]);
    if (![self isNullOrEmpty:[DBM loadTokenFromDB]] && [DBM hasConnectivity]) {
        [self performSegueWithIdentifier:@"show" sender:self];
    }
    
//    FBSDKLoginButton *loginButton = [[FBSDKLoginButton alloc] init];
//    loginButton.center = self.view.center;
//    [self.view addSubview:loginButton];
    
    UITapGestureRecognizer *tapGestureWordpress =
    [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(LogIn:)];
    [_LoginLB addGestureRecognizer:tapGestureWordpress];
    
    UITapGestureRecognizer *tapGestureFB =
    [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(LogInFB:)];
    [_LoginFBLB addGestureRecognizer:tapGestureFB];
    
    UITapGestureRecognizer *tapGestureLin =
    [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(LogInLinkedin:)];
    [_LoginLinLB addGestureRecognizer:tapGestureLin];
    
    if (![DBM hasConnectivity] && ![self isNullOrEmpty:[DBM loadTokenFromDB]]){
        NSLog(@"niepolaczony");
        [self performSegueWithIdentifier:@"offline" sender:self];
    }else{
        NSLog(@"polaczony");
    }
    
}

- (IBAction)goToWeb:(id)sender {
    
    NSString *reg = @"http://polanditmeeting.com/register//";
    NSString *url = [NSString stringWithFormat:@"%@",reg];
    url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:url]];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == _PasswordTF) {
        [self LogIn:self];
        [textField resignFirstResponder];
    } else if (textField == _LoginTF) {
        [_PasswordTF becomeFirstResponder];
    }
    return YES;
    return NO;
}



@end
