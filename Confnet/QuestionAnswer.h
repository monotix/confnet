//
//  QuestionAnswer.h
//  Confnet
//
//  Created by Rafał Osowicki on 03.08.2015.
//  Copyright (c) 2015 Rafał Osowicki. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Participant, ValueAnswer;

@interface QuestionAnswer : NSManagedObject

@property (nonatomic, retain) NSString * answerQuestion;
@property (nonatomic, retain) NSSet *answerID;
@property (nonatomic, retain) Participant *partycipantID;
@end

@interface QuestionAnswer (CoreDataGeneratedAccessors)

- (void)addAnswerIDObject:(ValueAnswer *)value;
- (void)removeAnswerIDObject:(ValueAnswer *)value;
- (void)addAnswerID:(NSSet *)values;
- (void)removeAnswerID:(NSSet *)values;

@end
