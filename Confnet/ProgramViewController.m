//
//  ProgramViewController.m
//  Confnet
//
//  Created by Rafał Osowicki on 02.07.2015.
//  Copyright (c) 2015 Rafał Osowicki. All rights reserved.
//

#import "ProgramViewController.h"
#import "CellController.h"
#import "JsonManager.h"
#import "Program.h"
#import "AppDelegate.h"
#import <CoreData/CoreData.h>
#include "DetailViewController.h"

@class JsonManager;
@interface ProgramViewController(){
    NSArray *JsonMsg;
    NSMutableArray *ProgramList;
}
@end

@implementation ProgramViewController

static NSString *CellIdentifier= @"ProgramCell";
@synthesize tableView;



- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIImage* image3 = [UIImage imageNamed:@"ic_action_logout.png"];
    CGRect frameimg = CGRectMake(15,5, 30,30);
    UIButton *logoutButton = [[UIButton alloc] initWithFrame:frameimg];
    [logoutButton setBackgroundImage:image3 forState:UIControlStateNormal];
    [logoutButton addTarget:self action:@selector(Logout)
           forControlEvents:UIControlEventTouchUpInside];
    [logoutButton setShowsTouchWhenHighlighted:YES];
    
    UIBarButtonItem *logoutButton1 =[[UIBarButtonItem alloc] initWithCustomView:logoutButton];
    UINavigationItem *navItem = [[UINavigationItem alloc] init];
    navItem.rightBarButtonItem = logoutButton1;
    navItem.title = @"Program";
    super.bar.items = @[ navItem ];
    
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl setTintColor:[UIColor colorWithRed:217.0f/255.0f green:11.0f/255.0f blue:140.0f/255.0f alpha:1.0]];
    [refreshControl tintColorDidChange];
    [refreshControl removeFromSuperview];
    [tableView addSubview:refreshControl];
    //refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Pull to Refresh"];
    [refreshControl addTarget:self action:@selector(ListInit) forControlEvents:UIControlEventValueChanged];
    
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeLeft:)];
    swipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipeLeft];
    swipeLeft.delegate = self;
    
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRight:)];
    swipeRight.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swipeRight];
    swipeRight.delegate = self;
    
    _activityindicator = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(150, 200, 30, 30)];
    [_activityindicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [_activityindicator setColor:[UIColor colorWithRed:217.0f/255.0f green:11.0f/255.0f blue:140.0f/255.0f alpha:1.0]];
    _activityindicator.center = self.view.center;

}

-(void)viewDidAppear:(BOOL)animated{
    
    
    [super viewWillAppear:animated];
    
    [self.view addSubview:_activityindicator];
    
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView = (id)[self.view viewWithTag:1];
    [tableView registerClass:[CellController class] forCellReuseIdentifier:CellIdentifier];
    tableView.backgroundColor = [UIColor colorWithRed:0.93 green:0.93 blue:0.93 alpha:1.0];
    [tableView addSubview:refreshControl];
    
    UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 70)];
    footer.backgroundColor = [UIColor clearColor];
    tableView.tableFooterView = footer;
    
    tableView.allowsSelection = NO;
    
    DBM = [DBManager sharedManager];
    if (![DBM hasConnectivity]) {
        if (!ListDone) {
    
        ProgramList = [[NSMutableArray alloc] init];
        ProgramList = [DBM loadProgramFromDB];
            [tableView reloadData];
        NSLog(@"Program %@", ProgramList);
        }
    }else{
    NSLog(@"Program %@ %d", ProgramList, ListDone);
    if(!ListDone)
        [self ListInit];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
   
}


-(void)ListInit{
    
    if([DBM hasConnectivity]){
    
    [DBM deleteProgramFromDB];
    if(!ListDone)
        [_activityindicator startAnimating];
    
    NSString *address = [NSString stringWithFormat:@"http://polanditmeeting.com/wp-json/cn/program"];
    JManager = [[JsonManager alloc] init];
    [JManager getJSONByAddress:address Completion:^(BOOL success) {
        if(success){
            programCount = [JManager getDayCount];
    ProgramList = [[NSMutableArray alloc] init];
        
    for(int i = 0; i<programCount; i++){
        NSMutableArray *hours = [[NSMutableArray alloc] init];
        NSMutableArray *valueList = [[NSMutableArray alloc] init];;
        for(int j=0;j<[JManager getDayProgramCount:i];j++)
        {
            [hours addObject:[JManager getHourInDay:i AtIndex:j]];
            [valueList addObject:[JManager getDetailValueInDay:i AtIndex:j]];
            NSLog(@"value: %@", [JManager getDetailValueInDay:i AtIndex:j]);
        }
        NSLog(@"full date:%@",[JManager getFullDateAtIndex:i]);
        Program *p = [[Program alloc] initWithFullDay:[JManager getFullDateAtIndex:i] DayHuman:[JManager getDayHumanAtIndex:i] Hours:hours DetailValue:valueList];
        [ProgramList addObject:p];
        
    }
        [self refreshData:tableView];
        if(ListDone)
            [self refreshTable];
        [_activityindicator stopAnimating];
        [tableView setUserInteractionEnabled:YES];
        ListDone = YES;
            [refreshControl endRefreshing];
       
                [DBM saveProgramToDB:ProgramList];
            
        }else{
            [_activityindicator stopAnimating];
            [tableView reloadData];
            [refreshControl endRefreshing];
            return;
        }
        
        
    }];
        
    }else{
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Network" message: @"No internet connection." delegate: self
                                             cancelButtonTitle:@"OK"
                                             otherButtonTitles:nil,nil];
        
        [alert show];
        
        [refreshControl endRefreshing];
    }
}

- (void)refreshTable {
    [refreshControl endRefreshing];
    //[refreshControl removeFromSuperview];
    [tableView reloadData];
}

-(void)refreshData:(UITableView *)t{
    [t reloadData];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if ([ProgramList count] != 0) {
        
        self.tableView.backgroundView = nil;
        return [ProgramList count];
        
    } else {
        
        UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
        
        messageLabel.text = @"No data is currently available. Please pull down to refresh.";
        messageLabel.textColor = [UIColor blackColor];
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = NSTextAlignmentCenter;
        messageLabel.font = [UIFont systemFontOfSize:16];
        [messageLabel sizeToFit];
        
        self.tableView.backgroundView = messageLabel;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CellController *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil){
        cell = [[CellController alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }else{
        
        cell.layer.shadowOffset = CGSizeMake(1, 0);
        cell.layer.shadowColor = [[UIColor blackColor] CGColor];
        cell.layer.shadowRadius = 4;
        cell.layer.shadowOpacity = .25;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        
        cell.titleDate = [[ProgramList objectAtIndex:indexPath.section] day_human];
        cell.value = [[ProgramList objectAtIndex:indexPath.section] detailValue];
        cell.hours = [[ProgramList objectAtIndex:indexPath.section] hours];
    }
    
    return cell;
}

-(NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    return indexPath;
}

-(void)backToList{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    BarMenuViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"detail"];
    NSString *viewControllerName = [[NSString alloc] initWithString:topController.nibName];
    NSString *viewControllerName2 = [[NSString alloc] initWithString:vc.nibName];
    if(![viewControllerName isEqualToString:viewControllerName2])[topController presentViewController:vc animated:YES completion:nil];
}

-(void) swipeRight:(UISwipeGestureRecognizer *) recognizer {
    if (recognizer.direction == UISwipeGestureRecognizerDirectionRight)
        [self.tabBarController setSelectedIndex:[self.tabBarController selectedIndex] - 1];
}

-(void) swipeLeft:(UISwipeGestureRecognizer *) recognizer {
    if (recognizer.direction == UISwipeGestureRecognizerDirectionLeft)
        [self.tabBarController setSelectedIndex:[self.tabBarController selectedIndex] + 1];
    
}



@end
