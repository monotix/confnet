//
//  Program.m
//  Confnet
//
//  Created by Rafał Osowicki on 06.07.2015.
//  Copyright (c) 2015 Rafał Osowicki. All rights reserved.
//

#import "Program.h"

@implementation Program
@synthesize hours=_hours,detailValue=_detailValue,dayDateObj=_day,day_human;


-(id)initWithFullDay:(NSDate*)fd DayHuman:(NSString*)d Hours:(NSMutableArray *)hour DetailValue:(NSMutableArray *)detail{
    
    if(self){
        self.dayDateObj = fd;
        self.day_human = d;
        self.hours = hour;
        self.detailValue = detail;
    }
    
    return self;
}

-(id)initWithProgram:(Program*)program{
    
    if(self){
        self.dayDateObj = program.dayDateObj;
        self.day_human = program.day_human;
        self.hours = program.hours;
        self.detailValue = program.detailValue;
    }
    
    return self;
}

@end
