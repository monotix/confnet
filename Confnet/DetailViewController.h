//
//  DetailViewController.h
//  Confnet
//
//  Created by Rafał Osowicki on 02.07.2015.
//  Copyright (c) 2015 Rafał Osowicki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BarMenuViewController.h"
#import "Participants.h"
#import <MessageUI/MessageUI.h>
#import "DBManager.h"

@class JsonManager;
@interface DetailViewController : BarMenuViewController<MFMailComposeViewControllerDelegate>{
    JsonManager *JManager;
    
    Participants *participant;
    DBManager *DBM;
    UIScrollView *scrollView;
    UITabBarController *tbc;
    NSString *categories;
    NSArray *details;
    NSString *detailStr;
    int index;
    UIImage *imageAV;
    NSMutableArray *ParticipantsList;
}

@property(nonatomic) CGRect screenBound;
@property(nonatomic, retain) Participants *participant;
-(void)setParticipant:(Participants*)par AndIndex:(int)index Image:(UIImage*)img PartList:(NSMutableArray *)PL;
@property(nonatomic,assign) id<MFMailComposeViewControllerDelegate> mailComposeDelegate;
@property (nonatomic, strong) UIActivityIndicatorView *activityindicator;
@end
