//
//  DBManager.m
//  Confnet
//
//  Created by Rafał Osowicki on 06.07.2015.
//  Copyright (c) 2015 Rafał Osowicki. All rights reserved.
//

#import "DBManager.h"
#import "AppDelegate.h"
#import <CoreData/CoreData.h>
#import "Program.h"
#import "Participants.h"
#import "Participant.h"
#import "ParticipantDB.h"
#import "Question.h"
#import "Answer.h"
#import "MeetingsDB.h"
#import "Meeting.h"
#import "DBProgram.h"
#import "DayProgram.h"

@implementation DBManager

+(id)sharedManager{
    
    static DBManager *sharedDBManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedDBManager = [[self alloc] init];
    });
    return sharedDBManager;
}

-(id)init{
    
    if(self = [super init]){
        appDelegate = [[UIApplication sharedApplication] delegate];
        context = [appDelegate managedObjectContext];
        JManager = [[JsonManager alloc] init];
    }
    
    return self;
}

-(void)saveTokenToDB:(NSString*)tkn{
    
    
    NSManagedObject *newToken;
    newToken = [NSEntityDescription insertNewObjectForEntityForName:@"Token" inManagedObjectContext:context];
    [newToken setValue:tkn forKey:@"token"];
    NSError *error;
    [context save:&error];
}

-(NSString*)loadTokenFromDB{
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Token" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;
    NSArray *result = [context executeFetchRequest:fetchRequest error:&error];
    NSArray *tokenn = [result valueForKeyPath:@"token"];
    NSError *saveError = nil;
    [context save:&saveError];
    NSLog(@"%@", tokenn);
    if([tokenn isKindOfClass:NULL] || ![tokenn count])
        return nil;
    
    return [tokenn objectAtIndex:0];
    
}

-(void)saveParticipantsToDB:(NSMutableArray *) parList{

    [self deleteParticipanFromDB];
    NSLog(@"Save");
    
        NSEntityDescription *entityParticipant = [NSEntityDescription entityForName:@"ParticipantDB" inManagedObjectContext:context];
        NSEntityDescription *entityQuestionAnswer = [NSEntityDescription entityForName:@"Question" inManagedObjectContext:context];
        NSEntityDescription *entityValueAnswer = [NSEntityDescription entityForName:@"Answer" inManagedObjectContext:context];
    
        for(int i=0; i < [parList count]; i++){
            par = [[Participants alloc] initWithParticipant:[parList objectAtIndex:i]];
    
            NSManagedObject *newParticipant = [[NSManagedObject alloc] initWithEntity:entityParticipant insertIntoManagedObjectContext:context];
            [newParticipant setValue: par.userName forKey:@"name"];
            [newParticipant setValue:par.usrID forKey:@"userID"];
            [newParticipant setValue:par.avatarURL forKey:@"avatarURL"];
            [newParticipant setValue:par.company forKey:@"company"];
            [newParticipant setValue:par.companyPosition forKey:@"companyPosition"];
            [newParticipant setValue:par.email forKey:@"email"];
            [newParticipant setValue:par.phone1 forKey:@"phone1"];
            [newParticipant setValue:par.phone2 forKey:@"phone2"];
            [newParticipant setValue:par.usrID forKey:@"paid"];
            
        
            NSError *error = nil;
            if (![newParticipant.managedObjectContext save:&error]) {
                NSLog(@"Unable to save managed object context.");
                NSLog(@"%@, %@", error, error.localizedDescription);
            }
    
            //NSManagedObject *newQuestionAnswer = [[NSManagedObject alloc] initWithEntity:entityQuestionAnswer insertIntoManagedObjectContext:context];
            //NSManagedObject *newValueAnswer = [[NSManagedObject alloc] initWithEntity:entityValueAnswer insertIntoManagedObjectContext:context];
            
            for (int j = 0; j < [par.questionnaire count]; j++) {
                
                NSManagedObject *newQuestionAnswer = [[NSManagedObject alloc] initWithEntity:entityQuestionAnswer insertIntoManagedObjectContext:context];
                NSManagedObject *newValueAnswer = [[NSManagedObject alloc] initWithEntity:entityValueAnswer insertIntoManagedObjectContext:context];
                
                [newQuestionAnswer setValue:[par.keys objectAtIndex:j] forKey:@"question"];
                [newQuestionAnswer setValue: par.usrID forKey:@"userID"];
                [newQuestionAnswer setValue:[par.keys objectAtIndex:j] forKey:@"answerID"];
                
                if (![newQuestionAnswer.managedObjectContext save:&error]) {
                    NSLog(@"Unable to save managed object context.");
                    NSLog(@"%@, %@", error, error.localizedDescription);
                }
                
                
                
                if ([[par.questionnaire valueForKey:[par.keys objectAtIndex:j]] isKindOfClass:[NSArray class]]){
                    
                    NSArray *tab = [[NSArray alloc] initWithArray:[par.questionnaire valueForKey:[par.keys objectAtIndex:j]]];
                
                                        for (int k = 0; k < [tab count]; k++) {
                
                                            NSManagedObject *newValueAnswer = [[NSManagedObject alloc] initWithEntity:entityValueAnswer insertIntoManagedObjectContext:context];
                                            [newValueAnswer setValue:[tab objectAtIndex:k]  forKey:@"answer"];
                
                                            [newValueAnswer setValue:[par.keys objectAtIndex:j] forKey:@"questionID"];
                                            [newValueAnswer setValue:par.usrID forKey:@"userID"];
                
                                            NSError *error = nil;
                                            if (![newValueAnswer.managedObjectContext save:&error]) {
                                                NSLog(@"Unable to save managed object context.");
                                                NSLog(@"%@, %@", error, error.localizedDescription);
                                            }
                                        }
                
                } else {
                                        [newValueAnswer setValue:[par.questionnaire valueForKey:[par.keys objectAtIndex:j]] forKey:@"answer"];
                
                                        [newValueAnswer setValue:[par.keys objectAtIndex:j] forKey:@"questionID"];
                                        [newValueAnswer setValue:par.usrID forKey:@"userID"];
                
                                        NSError *error = nil;
                                        if (![newValueAnswer.managedObjectContext save:&error]) {
                                            NSLog(@"Unable to save managed object context.");
                                            NSLog(@"%@, %@", error, error.localizedDescription);
                                        }
                
                                    }
                
            }
            
        }
    
      
}
    
    
-(NSMutableArray *)loadParticipantsFromDB{
    
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity1 = [NSEntityDescription entityForName:@"Question" inManagedObjectContext:context];
    [fetchRequest setEntity:entity1];
    
    NSError* error;
    NSArray *questList = [context executeFetchRequest:fetchRequest error:&error];
    
    NSEntityDescription *entity2 = [NSEntityDescription entityForName:@"Answer" inManagedObjectContext:context];
    [fetchRequest setEntity:entity2];
    
    NSArray *answerList = [context executeFetchRequest:fetchRequest error:&error];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"ParticipantDB" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"company" ascending:YES];
    [fetchRequest setSortDescriptors:@[sortDescriptor]];
    
    
    NSArray *partList = [context executeFetchRequest:fetchRequest error:&error];
    
    
    
  
    
    NSMutableArray *ParticipantList = [[NSMutableArray alloc] init];
    for (int i = 0; i < [partList count]; i++) {
        ParticipantDB* part = [partList objectAtIndex:i];
        Participants *p = [[Participants alloc] initWithUserName:part.name UserID:part.userID Email:part.email Company:part.company CompanyPosition:part.companyPosition Phone1:part.phone1 Phone2:part.phone2 UserPaid:part.paid AvatarURL:part.avatarURL Questionnaire:nil Keys:nil];
        
        [ParticipantList addObject:p];
        
        
        NSMutableArray *key = [[NSMutableArray alloc] init];
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        
        for (int j = 0; j < [questList count]; j++) {
            Question *q = [questList objectAtIndex:j];
           
            
            
            
            
            if ([p.usrID isEqualToString:q.userID]){
                [key addObject:q.question];
            
            NSMutableArray *value = [[NSMutableArray alloc] init];
                NSArray *tab;
            
            for (int k = 0; k < [answerList count]; k++) {
                
                Answer *a = [answerList objectAtIndex:k];
            //NSLog(@"answery %@ %@ %@ %@ %@", q.userID, a.userID, a.questionID,q.answerID,a.answer);
                
                if ([a.questionID isEqualToString: q.answerID] && [a.userID isEqualToString:q.userID]) {
                    [value addObject:a.answer];
                    //NSLog(@"answery %@ %@ %@ %@", value , q.answerID,a.userID,q.userID);
                }
            }
                tab = [[NSArray alloc] initWithArray:value];
                [dict setObject:tab forKey:[key objectAtIndex:[key count] - 1]];
            }
         
        }
        p.keys = key;
        p.questionnaire = dict;
        
       }
    
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"company" ascending:YES selector:@selector(caseInsensitiveCompare:)];
    [ParticipantList sortUsingDescriptors:[NSArray arrayWithObject:sort]];
    
    return ParticipantList;
    
}

-(void)saveMeetingsToDB:(NSMutableArray *)meetingsList{
    
    [self deleteMeetingsFromDB];
    
    for (int i = 0; i < [meetingsList count]; i++) {
        Meeting *m = [[Meeting alloc] initWithMeeting:[meetingsList objectAtIndex:i]];
        NSManagedObject *newMeeting;
        newMeeting = [NSEntityDescription insertNewObjectForEntityForName:@"MeetingsDB" inManagedObjectContext:context];
        [newMeeting setValue:m.time forKey:@"time"];
        [newMeeting setValue:m.usrID forKey:@"userID"];
        [newMeeting setValue:m.inviteState forKey:@"status"];
        [newMeeting setValue:m.msg forKey:@"message"];
        NSError *error;
        [context save:&error];
    }
}

-(NSMutableArray *)loadMeetingsFromDB{
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity1 = [NSEntityDescription entityForName:@"MeetingsDB" inManagedObjectContext:context];
    [fetchRequest setEntity:entity1];
    
    NSError *error;
    NSArray *meetingsList = [context executeFetchRequest:fetchRequest error:&error];
    NSMutableArray *ML = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < [meetingsList count]; i++) {
        MeetingsDB *m = [meetingsList objectAtIndex:i];
        Meeting *meet = [[Meeting alloc] initWithMid:0 InviteState:m.status Time:m.time Message:m.message UserID:m.userID];
        
        [ML addObject:meet];
    }
    NSLog(@"ML %@",ML);
    
    return ML;
}

-(void)saveProgramToDB:(NSMutableArray *)programList{
    
    NSError *error;
    
    for (int i = 0; i < [programList count]; i++) {
        Program *p = [[Program alloc] initWithProgram:[programList objectAtIndex:i]];
        NSLog(@"czy wchodzi? %@", p.dayDateObj);
        NSManagedObject *newDBProgram;
        newDBProgram = [NSEntityDescription insertNewObjectForEntityForName:@"DBProgram" inManagedObjectContext:context];
        [newDBProgram setValue:p.dayDateObj forKey:@"dateProgram"];
        [newDBProgram setValue:p.day_human forKey:@"dayHuman"];
        NSLog(@"day program data: %@",p.dayDateObj);
        [newDBProgram.managedObjectContext save:&error];
        for (int j = 0; j < [p.hours count]; j++) {
            NSManagedObject *newDayProgram;
            newDayProgram = [NSEntityDescription insertNewObjectForEntityForName:@"DayProgram" inManagedObjectContext:context];
            [newDayProgram setValue:p.dayDateObj forKey:@"date"];
            [newDayProgram setValue:[p.hours objectAtIndex:j] forKey:@"hours"];
            [newDayProgram setValue:[p.detailValue objectAtIndex:j] forKey:@"detailValue"];
            [newDayProgram.managedObjectContext save:&error];       }
    }

    [context save:&error];
}

-(NSMutableArray *)loadProgramFromDB{
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity1 = [NSEntityDescription entityForName:@"DayProgram" inManagedObjectContext:context];
    [fetchRequest setEntity:entity1];
    
    NSError *error;
    NSArray *DayProgramList = [context executeFetchRequest:fetchRequest error:&error];
    
    NSEntityDescription *entity2 = [NSEntityDescription entityForName:@"DBProgram" inManagedObjectContext:context];
    [fetchRequest setEntity:entity2];
    
    NSArray *DBProgramList = [context executeFetchRequest:fetchRequest error:&error];
    NSMutableArray *ProgramList = [[NSMutableArray alloc] init];
    
    for(NSManagedObject *obj in DayProgramList){
        NSLog(@"daaaa : %@",obj);
    }
    for (int i = 0; i < [DBProgramList count]; i++) {
        DBProgram *dpl = [DBProgramList objectAtIndex:i];
        Program *p = [[Program alloc] initWithFullDay:dpl.dateProgram DayHuman:dpl.dayHuman Hours:nil DetailValue:nil];
        
        
        NSMutableArray *hourList = [[NSMutableArray alloc] init];
        NSMutableArray *detailList = [[NSMutableArray alloc] init];
        
        for (int j = 0; j < [DayProgramList count]; j++) {
            DayProgram *dp = [DayProgramList objectAtIndex:j];
            NSLog(@"day program: %@, %@",dp.date,dpl.dateProgram);
            if([dp.date isEqualToDate: dpl.dateProgram]){
                [hourList addObject:dp.hours];
                [detailList addObject:dp.detailValue];
                NSLog(@"day program: %@, %@",dp.hours,dp.detailValue);
            }
            
        }
        
        p.detailValue = detailList;
        p.hours = hourList;
        [ProgramList addObject:p];
        
        
        
    }
    
    
    return ProgramList;
}

-(void)deleteParticipanFromDB{
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity1 = [NSEntityDescription entityForName:@"Question" inManagedObjectContext:context];
    [fetchRequest setEntity:entity1];
    
    NSError* error;
    NSArray *questList = [context executeFetchRequest:fetchRequest error:&error];
    
    for (NSManagedObject * obj in questList) {
        [context deleteObject:obj];
    }
    
    NSEntityDescription *entity2 = [NSEntityDescription entityForName:@"Answer" inManagedObjectContext:context];
    [fetchRequest setEntity:entity2];
    
    NSArray *answerList = [context executeFetchRequest:fetchRequest error:&error];
    
    for (NSManagedObject * obj in answerList) {
        [context deleteObject:obj];
    }
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"ParticipantDB" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"company" ascending:YES];
    [fetchRequest setSortDescriptors:@[sortDescriptor]];
    
    
    NSArray *partList = [context executeFetchRequest:fetchRequest error:&error];
    
    
   
    for (NSManagedObject * obj in partList) {
        [context deleteObject:obj];
    }
}

-(void)deleteMeetingsFromDB{
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSError* error;
    NSEntityDescription *entity1 = [NSEntityDescription entityForName:@"MeetingsDB" inManagedObjectContext:context];
    [fetchRequest setEntity:entity1];
    
    NSArray *meet = [context executeFetchRequest:fetchRequest error:&error];
    
    for (NSManagedObject * obj in meet) {
        [context deleteObject:obj];
    }
    
    
    
    
}

-(void)deleteProgramFromDB{
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSError* error;
    NSEntityDescription *entity1 = [NSEntityDescription entityForName:@"DBProgram" inManagedObjectContext:context];
    [fetchRequest setEntity:entity1];
    
    NSArray *prog = [context executeFetchRequest:fetchRequest error:&error];
    
    for (NSManagedObject * obj in prog) {
        [context deleteObject:obj];
    }
    
    NSEntityDescription *entity2 = [NSEntityDescription entityForName:@"DayProgram" inManagedObjectContext:context];
    [fetchRequest setEntity:entity2];
    
    NSArray *day = [context executeFetchRequest:fetchRequest error:&error];
    
    for (NSManagedObject * obj in day) {
        [context deleteObject:obj];
    }
    
    
    
    
}

-(void)deleteToken{
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Token" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    
    NSError *error = nil;
    NSArray *result = [context executeFetchRequest:fetchRequest error:&error];
    for (NSManagedObject * obj in result) {
        [context deleteObject:obj];
    }
}

-(BOOL)internetConnection{
    return offline;
}

-(void)setinternetConnection:(BOOL)b{
    offline = b;
}

-(BOOL)hasConnectivity {
    struct sockaddr_in zeroAddress;
    bzero(&zeroAddress, sizeof(zeroAddress));
    zeroAddress.sin_len = sizeof(zeroAddress);
    zeroAddress.sin_family = AF_INET;
    
    SCNetworkReachabilityRef reachability = SCNetworkReachabilityCreateWithAddress(kCFAllocatorDefault, (const struct sockaddr*)&zeroAddress);
    if(reachability != NULL) {
        SCNetworkReachabilityFlags flags;
        if (SCNetworkReachabilityGetFlags(reachability, &flags)) {
            if ((flags & kSCNetworkReachabilityFlagsReachable) == 0)
            {
                
                return NO;
            }
            
            if ((flags & kSCNetworkReachabilityFlagsConnectionRequired) == 0)
            {
                return YES;
            }
            
            
            if ((((flags & kSCNetworkReachabilityFlagsConnectionOnDemand ) != 0) ||
                 (flags & kSCNetworkReachabilityFlagsConnectionOnTraffic) != 0))
            {
                if ((flags & kSCNetworkReachabilityFlagsInterventionRequired) == 0)
                {
                    return YES;
                }
            }
            
            if ((flags & kSCNetworkReachabilityFlagsIsWWAN) == kSCNetworkReachabilityFlagsIsWWAN)
            {
                return YES;
            }
        }
    }
    
    return NO;
}

-(void)saveImage: (UIImage*)image WithName:(NSString*)name{
    if (image != nil)
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                             NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:name ];
        NSData* data = UIImagePNGRepresentation(image);
        [data writeToFile:path atomically:YES];
    }
}

-(UIImage*)loadImage:(NSString*)name {
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:name ];
    UIImage* image = [UIImage imageWithContentsOfFile:path];
    if (image == nil) {
        image = [UIImage imageNamed:@"avatar_placeholder.png"];
    }
    return image;
}

-(void)removeImg{
    
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSArray *fileArray = [fileMgr contentsOfDirectoryAtPath:documentsDirectory error:nil];
    for (NSString *filename in fileArray)  {
        NSLog(@"file name %@", filename);
        if (![filename isEqualToString:@"Model.sqlite"] && ![filename isEqualToString:@"Model.sqlite-wal"] && ![filename isEqualToString:@"Model.sqlite-shm"]) {
            [fileMgr removeItemAtPath:[documentsDirectory stringByAppendingPathComponent:filename] error:NULL];
        }
        
    }
}


//-(void)saveParticipantsToDB:(NSMutableArray *) parList{
//    
//    NSLog(@"Sejwujesz? ");
//    NSEntityDescription *entityParticipant = [NSEntityDescription entityForName:@"Participant" inManagedObjectContext:context];
//    NSEntityDescription *entityQuestionAnswer = [NSEntityDescription entityForName:@"QuestionAnswer" inManagedObjectContext:context];
//    NSEntityDescription *entityValueAnswer = [NSEntityDescription entityForName:@"ValueAnswer" inManagedObjectContext:context];
//    
//    for(int i=0; i < [parList count]; i++){
//        par = [[Participants alloc] initWithParticipant:[parList objectAtIndex:i]];
//        
//        NSManagedObject *newParticipant = [[NSManagedObject alloc] initWithEntity:entityParticipant insertIntoManagedObjectContext:context];
//        [newParticipant setValue: par.userName forKey:@"user_name"];
//        [newParticipant setValue:par.usrID forKey:@"userID"];
//        [newParticipant setValue:par.avatarURL forKey:@"avatarURL"];
//        [newParticipant setValue:par.company forKey:@"company"];
//        [newParticipant setValue:par.companyPosition forKey:@"company_position"];
//        [newParticipant setValue:par.email forKey:@"email"];
//        [newParticipant setValue:par.phone1 forKey:@"phone1"];
//        [newParticipant setValue:par.phone2 forKey:@"phone2"];
//        [newParticipant setValue:par.usrID forKey:@"user_paid"];
//        
//        NSManagedObject *newQuestionAnswer = [[NSManagedObject alloc] initWithEntity:entityQuestionAnswer insertIntoManagedObjectContext:context];
//        NSManagedObject *newValueAnswer = [[NSManagedObject alloc] initWithEntity:entityValueAnswer insertIntoManagedObjectContext:context];
//        //NSLog(@"par ::%@", par.questionnaire );
//        NSMutableSet *AnswerList = [newParticipant mutableSetValueForKey:@"user_id"];
//        
//                for (int j = 0; j < [par.questionnaire count]; j++) {
//        
//                    [newQuestionAnswer setValue:[par.keys objectAtIndex:j] forKey:@"answerQuestion"];
//                    [AnswerList addObject:newQuestionAnswer];
//                   // NSLog(@"Answer: %@ %@",AnswerList,newQuestionAnswer);
//                    
//                    
//                    if ([[par.questionnaire valueForKey:[par.keys objectAtIndex:j]] isKindOfClass:[NSArray class]]){
//                    
//                    NSArray *tab = [[NSArray alloc] initWithArray:[par.questionnaire valueForKey:[par.keys objectAtIndex:j]]];
//
//                        
//                        for (int k = 0; k < [tab count]; k++) {
//                            [newValueAnswer setValue:[tab objectAtIndex:k]  forKey:@"value"];
//                         
//                            [newQuestionAnswer setValue:[NSSet setWithObject:newValueAnswer] forKey:@"answerID"];
//                            
//                            NSError *error = nil;
//                            if (![newQuestionAnswer.managedObjectContext save:&error]) {
//                                NSLog(@"Unable to save managed object context.");
//                                NSLog(@"%@, %@", error, error.localizedDescription);
//                            }
//                        }
//                        
//                    } else {
//                        [newValueAnswer setValue:[par.questionnaire valueForKey:[par.keys objectAtIndex:j]] forKey:@"value"];
//                        
//                        [newQuestionAnswer setValue:[NSSet setWithObject:newValueAnswer] forKey:@"answerID"];
//                        
//                        NSError *error = nil;
//                        if (![newQuestionAnswer.managedObjectContext save:&error]) {
//                            NSLog(@"Unable to save managed object context.");
//                            NSLog(@"%@, %@", error, error.localizedDescription);
//                        }
//                        
//                    }
//                    
//                    
//                }
//        
//        if ([par.questionnaire count] == 0) {
//            
//            [newQuestionAnswer setValue:@"null" forKey:@"answerQuestion"];
//            [AnswerList addObject:newQuestionAnswer];
//            
//            [newValueAnswer setValue:@"null"  forKey:@"value"];
//            
//            [newQuestionAnswer setValue:[NSSet setWithObject:newValueAnswer] forKey:@"answerID"];
//            
//            NSError *error = nil;
//            [newQuestionAnswer.managedObjectContext save:&error];
//            [newValueAnswer.managedObjectContext save:&error];
//            
//            
//        }
//        // Add
//        [newParticipant setValue:AnswerList forKey:@"user_id"];
//        NSLog(@"new part: %@", newParticipant);
//        // Save
//        NSError *error = nil;
//        if (![newParticipant.managedObjectContext save:&error]) {
//            NSLog(@"Unable to save managed object context.");
//            NSLog(@"%@, %@", error, error.localizedDescription);
//        }
//    }
//    
//    [self loadParticipantsFromDB];
//}



//-(void)loadParticipantsFromDB{
//   
//    NSLog(@"ładuj!!!");
//    
//    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
//    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Participant" inManagedObjectContext:context];
//    [fetchRequest setEntity:entity];
//    
//    NSError* error;
//    NSArray* partList = [context executeFetchRequest:fetchRequest error:&error];
//    
//  
//    NSFetchRequest *fetchRequestItems = [[NSFetchRequest alloc] init];
//    NSEntityDescription *entityItem = [NSEntityDescription entityForName:@"QuestionAnswer" inManagedObjectContext:context];
//    [fetchRequestItems setEntity:entityItem];
//    
//    for (int i = 0; i < 5; i++) {
//    Participant* part = [partList objectAtIndex:i];
//        NSLog(@"%@",part.user_id.anyObject);
//    }
//    Participant* part = [partList objectAtIndex:1];
//    [fetchRequestItems setPredicate:[NSPredicate predicateWithFormat:@"ANY user_id == %@",part]];
//    //[fetchRequestItems setReturnsObjectsAsFaults:NO];
//    //NSLog(@"Part z bazy: %@", part);
//    NSArray *sortDescriptors = [NSArray arrayWithObjects:nil];
//    //[fetchRequestItems setSortDescriptors:sortDescriptors];
//    //[fetchRequest setPropertiesToFetch:@[@"user_id.answerQuestion"]];
//    
//    NSArray* Answer = [context executeFetchRequest:fetchRequestItems error:&error];
//    
//    NSLog(@"Zobaczmy: %@ answery: %@",part.user_name, fetchRequestItems);
//    
//
////    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"company" ascending:YES];
////    [fetchRequest setSortDescriptors:@[sortDescriptor]];
////
////    NSError *fetchError = nil;
////    NSArray *result = [context executeFetchRequest:fetchRequest error:&fetchError];
////    
////    if (!fetchError) {
////        for (NSManagedObject *managedObject in result) {
////            //NSMutableSet *tab = [managedObject valueForKey:@"user_id"];
////            NSLog(@"DAta base: >>>>>>>>>>>>>%@", [managedObject valueForKey:@"user_id"]);
////            //NSLog(@"Bier imie: %@", predicateID);
////            
////           
////        }
////        
////    } else {
////        NSLog(@"Error fetching data.");
////        NSLog(@"%@, %@", fetchError, fetchError.localizedDescription);
////    }
////    
////    // create a filter
////    NSPredicate *predicate
////    = [NSPredicate predicateWithFormat:@"ANY", @"*"];
////    
////    // create a query
////    NSFetchRequest *request
////    = [NSFetchRequest fetchRequestWithEntityName:@"Participant"];
////    
////    // set the filter on the query
////    request.predicate = predicate;
////    
////    // execute the query
////    NSArray *albums = [context executeFetchRequest:request error:nil];
////    NSLog(@"Daj mnie: %@", albums);
//}

-(void)dealloc{
    
}

@end
