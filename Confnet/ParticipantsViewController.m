//
//  ParticipantsViewController.m
//  Confnet
//
//  Created by Rafał Osowicki on 02.07.2015.
//  Copyright (c) 2015 Rafał Osowicki. All rights reserved.
//

#import "ParticipantsViewController.h"
#import "JsonManager.h"
#import "CellController.h"
#import "Participants.h"
#import "DetailViewController.h"


UIRefreshControl *refreshControll;
@implementation ParticipantsViewController

static NSString *CellIdentifier= @"ParticipantsCell";

@synthesize token;
@synthesize tableView;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    UIImage* image3 = [UIImage imageNamed:@"ic_action_logout.png"];
    CGRect frameimg = CGRectMake(15,5, 30,30);
    UIButton *logoutButton = [[UIButton alloc] initWithFrame:frameimg];
    [logoutButton setBackgroundImage:image3 forState:UIControlStateNormal];
    [logoutButton addTarget:self action:@selector(Logout)
         forControlEvents:UIControlEventTouchUpInside];
    [logoutButton setShowsTouchWhenHighlighted:YES];
    
    UIBarButtonItem *logoutButton1 =[[UIBarButtonItem alloc] initWithCustomView:logoutButton];
  
    UINavigationItem *navItem = [[UINavigationItem alloc] init];
    navItem.rightBarButtonItem = logoutButton1;
    navItem.title = @"Participants";
    super.bar.items = @[ navItem ];
    DBM = [DBManager sharedManager];
    
    refreshControll = [[UIRefreshControl alloc] init];
    [refreshControll setTintColor:[UIColor colorWithRed:217.0f/255.0f green:11.0f/255.0f blue:140.0f/255.0f alpha:1.0]];
    [refreshControll tintColorDidChange];
    //refreshControll.attributedTitle = [[NSAttributedString alloc] initWithString:@"Pull to Refresh"];
    [refreshControll addTarget:self action:@selector(initParticipants) forControlEvents:UIControlEventValueChanged];
    [tableView addSubview:refreshControll];
    
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeLeft:)];
    swipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipeLeft];
    swipeLeft.delegate = self;
    
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRight:)];
    swipeRight.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swipeRight];
    swipeRight.delegate = self;
    
    _activityindicator = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(150, 200, 30, 30)];
    [_activityindicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [_activityindicator setColor:[UIColor colorWithRed:217.0f/255.0f green:11.0f/255.0f blue:140.0f/255.0f alpha:1.0]];
    _activityindicator.center = self.view.center;
    
    
    

}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
   [self.view addSubview:_activityindicator];
    
    
    
    
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView = (id)[self.view viewWithTag:2];
    [tableView registerClass:[CellController class] forCellReuseIdentifier:CellIdentifier];
    tableView.backgroundColor = [UIColor colorWithRed:0.93 green:0.93 blue:0.93 alpha:1.0];
    [tableView addSubview:refreshControll];
    
    UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 70)];
    footer.backgroundColor = [UIColor clearColor];
    tableView.tableFooterView = footer;
    
    //NSLog(@"internet: %d %@", [DBM internetConnection], ListDone);
    
    ParticipantsList = [[NSMutableArray alloc] init];
    ParticipantsList = [DBM loadParticipantsFromDB];
    if(![DBM hasConnectivity]){
        
        if(!ListDone){
            [self FromDB];
        }
    }else{


    if(!ListDone){
        
        keyList = [[NSMutableArray alloc] init];
        ParticipantsList = [[NSMutableArray alloc] init];
        imageArray = [[NSMutableArray alloc] init];
        [self initParticipants];
        ListDone = YES;
        [tableView reloadData];
    }else if(ListDone && !allImg){
        //[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            [self downloadIMG];
        });
    }
    }
}

-(void)initParticipants{
    
 
    if([DBM hasConnectivity]){
       [tableView setUserInteractionEnabled:NO];
        
        [DBM removeImg];
        [DBM deleteParticipanFromDB];
        [keyList removeAllObjects];
        //[ParticipantsList removeAllObjects];
        //[imageArray removeAllObjects];
    
    if(!ListDone)
        [_activityindicator startAnimating];
    
    NSMutableArray *ParticipantsListTemp = [[NSMutableArray alloc] init];
    //[tableView setUserInteractionEnabled:NO];
        
    NSString *address = [NSString stringWithFormat:@"http://polanditmeeting.com/wp-json/cn/participants"];
    NSString *address2 = [NSString stringWithFormat:@"http://polanditmeeting.com/wp-json/cn/participants/detail/"];
    
    JManager = [[JsonManager alloc] init];
    token = [DBM loadTokenFromDB];
    [JManager getJSONByAddress:address WithToken:token Completion:^(BOOL success) {
            if(success){
                
                
                [keyList removeAllObjects];
                //[ParticipantsList removeAllObjects];
                //[imageArray removeAllObjects];
                
                for(int i=0; i < (int)[JManager getUserCount]; i++){
            
                    Participants *p = [[Participants alloc] initWithUserName:[JManager getUserNameAtIndex:i] UserID:[JManager getUserIDAtIndex:i] Email:nil Company:[JManager getCompanyAtIndex:i] CompanyPosition:nil Phone1:nil Phone2:nil UserPaid:nil AvatarURL:[JManager getAvatarURLAtIndex:i] Questionnaire:nil Keys:nil];
                    
                        [ParticipantsListTemp addObject:p];
                    
                    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"company" ascending:YES selector:@selector(caseInsensitiveCompare:)];
                    [ParticipantsListTemp sortUsingDescriptors:[NSArray arrayWithObject:sort]];
                    
                    UIImage *image1 = [UIImage imageNamed:@"avatar_placeholder.png"];
                    
                    int roznica = [imageArray count] - i;
                    if (ListDone && (roznica > 0))
                        [imageArray replaceObjectAtIndex:i withObject:image1];
                    else
                        [imageArray addObject:image1];
                    
                }
                ParticipantsList = [[NSMutableArray alloc] initWithArray:ParticipantsListTemp];
                
                //[tableView reloadData];
                wait(2);
                if (!bg) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                    bg = YES;
                    [self downloadIMG];
                    [tableView reloadData];
                    dispatch_async(dispatch_get_main_queue(), ^{ [tableView reloadData]; });
                    bg = NO;
                });
                }
                
                [DBM saveParticipantsToDB:ParticipantsList];
                [refreshControll endRefreshing];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [_activityindicator stopAnimating];
                [tableView reloadData];
                
                ListDone = YES;
                [refreshControll removeFromSuperview];
                [tableView setUserInteractionEnabled:YES];
                [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
                    });
                
                
            }else{
                NSLog(@"tutaj");
                [refreshControll endRefreshing];
                [_activityindicator stopAnimating];
                [tableView reloadData];
                ListDone = NO;
                sc = YES;
                return;
            }
     
        
        }];
        
    }else{
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Network" message: @"No internet connection." delegate: self
                                             cancelButtonTitle:@"OK"
                                             otherButtonTitles:nil,nil];
        
        [alert show];
        
        [refreshControll endRefreshing];
        
    }
    //[tableView reloadData];
}

-(void)downloadIMG{
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    for (int i = 0; i < [ParticipantsList count]; i++) {
        NSLog(@"image %d",i);
    NSURL *url = [NSURL URLWithString:[[ParticipantsList objectAtIndex:i] avatarURL]];
    NSData *data = [NSData dataWithContentsOfURL : url];
    UIImage *image = [UIImage imageWithData: data];
    UIImage *image1 = [UIImage imageNamed:@"avatar_placeholder.png"];
    
    if(![image class] == nil){
        [imageArray replaceObjectAtIndex:i withObject:image];
        [DBM saveImage:image WithName:[NSString stringWithFormat:@"%@.png",[[ParticipantsList objectAtIndex:i] usrID]]];
        
        
        
        //[tableView reloadSections:[NSIndexSet indexSetWithIndex:i] withRowAnimation:UITableViewRowAnimationAutomatic];
        //[tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:i]] withRowAnimation:UITableViewRowAnimationAutomatic];
        //[self refreshData:tableView];
    }else{
        [DBM saveImage:image1 WithName:[NSString stringWithFormat:@"%d.png",i]];
    }
        if (i == [ParticipantsList count] - 1) {
            allImg = YES;
        }
        
    }
    //dispatch_async(dispatch_get_main_queue(), ^{ [tableView reloadData]; });
    [tableView reloadData];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [self.tableView addSubview:refreshControll];
    //[self doInBackground];

}

-(void)doInBackground{
    [DBM deleteParticipanFromDB];
    NSString *address2 = [NSString stringWithFormat:@"http://polanditmeeting.com/wp-json/cn/participants/detail/"];
    userCount = [JManager getUserCount];
    
    for(int i = 0; i < (int)userCount;i++){
        [JManager getJSONByAddress:address2 WithToken:token UserID:[[ParticipantsList objectAtIndex:i] usrID] Completion:^(BOOL success) {
            
            Participants *p = [ParticipantsList objectAtIndex:i];
            p.email = [JManager getUserEmail];
            p.phone1 = [JManager getUserPhone1];
            p.phone2 = [JManager getUserPhone2];
            p.companyPosition = [JManager getcompanyPosition];
            
            quest = [[NSMutableDictionary alloc] init];
            keys = [[NSMutableArray alloc] init];
            for (int j = 0; j < [JManager getQuestCount]; j++) {
                [quest setObject:[JManager getValueArIndex:j] forKey:[JManager getDescriptionAtIndex:j]];
                [keys addObject:[JManager getDescriptionAtIndex:j]];
            }
            p.questionnaire = quest;
            p.keys = keys;
            [keyList addObject:keys];
            
            if(i >= (int)userCount-1){
                [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
                tableView.allowsSelection = NO;
                [refreshControll removeFromSuperview];
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                [DBM saveParticipantsToDB:ParticipantsList];
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                tableView.allowsSelection = YES;
                    
                    [self.tableView addSubview:refreshControll];
                });
                
            }
            
            
        }];

    }
}

-(void)loadIMG{
    
    NSString *docsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES) objectAtIndex:0];
    NSArray *docFiles = [[NSFileManager defaultManager]contentsOfDirectoryAtPath:docsDir error:NULL];
    
    for (NSString *fileName in docFiles) {
        //check to see if the file is a .png file
        if ([fileName hasSuffix:@".png"]) {
            NSString *fullPath = [docsDir stringByAppendingPathComponent:fileName];
            UIImage *loadedImage = [UIImage imageWithContentsOfFile:fullPath];
            NSLog(@"%@",loadedImage);
        }
    }
    
}


-(void)FromDB{
   
    [_activityindicator startAnimating];
    ParticipantsList = [[NSMutableArray alloc] init];
    ParticipantsList = [DBM loadParticipantsFromDB];
    imageArray = [[NSMutableArray alloc] init];
    
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"company" ascending:YES selector:@selector(caseInsensitiveCompare:)];
    [ParticipantsList sortUsingDescriptors:[NSArray arrayWithObject:sort]];
    
    NSLog(@"przyszlo?");
    for (int i = 0; i < [ParticipantsList count]; i++) {
        
        NSURL *url = [NSURL URLWithString:[[ParticipantsList objectAtIndex:i] avatarURL]];
        NSData *data = [NSData dataWithContentsOfURL : url];
        UIImage *image = [UIImage imageWithData: data];
        UIImage *image1 = [UIImage imageNamed:@"avatar_placeholder.png"];
        NSLog(@"image class %d %@ %@",[ParticipantsList count],image,image1);
        
        [imageArray addObject:[DBM loadImage:[NSString stringWithFormat:@"%@.png",[[ParticipantsList objectAtIndex:i] usrID]]]];
    }
    
    [tableView reloadData];
    ListDone = YES;
    [_activityindicator stopAnimating];
}

-(void)refreshView:(UIRefreshControl *)refresh {
    
}


- (void)refreshTable {
    [refreshControll endRefreshing];
    [tableView reloadData];
}

-(void)refreshData:(UITableView *)t{
    [t reloadData];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    // Return the number of sections.
    
    
    return 1;

}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
    if ([ParticipantsList count] != 0) {
        
        self.tableView.backgroundView = nil;
        return [ParticipantsList count];

        
        
    } else {
        
        
        
        UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
        
        NSString *text;
        if([DBM hasConnectivity] && [[JManager getError] isEqualToString:@"-1011"])
           text = [NSString stringWithFormat:@"You must pay first to see this content."];
        else
           text = [NSString stringWithFormat:@"No data is currently available. Please pull down to refresh."];
        
        messageLabel.text = text;
        messageLabel.textColor = [UIColor blackColor];
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = NSTextAlignmentCenter;
        messageLabel.font = [UIFont systemFontOfSize:16];
        [messageLabel sizeToFit];
        
        self.tableView.backgroundView = messageLabel;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CellController *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil){
        cell = [[CellController alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }else{
        cell.layer.shadowOffset = CGSizeMake(1, 0);
        cell.layer.shadowColor = [[UIColor blackColor] CGColor];
        cell.layer.shadowRadius = 2;
        cell.layer.shadowOpacity = .25;
        
        cell.userName = [[ParticipantsList objectAtIndex:indexPath.section] userName];
        cell.company = [[ParticipantsList objectAtIndex:indexPath.section] company];
        cell.avatarIMG.image = [imageArray objectAtIndex:indexPath.section];
        
       
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    DetailViewController *NVC = [self.storyboard instantiateViewControllerWithIdentifier:@"Detail"];
    [NVC setParticipant:[ParticipantsList objectAtIndex:indexPath.section]AndIndex:indexPath.section Image:[DBM loadImage:[NSString stringWithFormat:@"%d.png",indexPath.section]]PartList:ParticipantsList ];
    [self presentViewController:NVC animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    return YES;
}

-(void) swipeRight:(UISwipeGestureRecognizer *) recognizer {
    if (recognizer.direction == UISwipeGestureRecognizerDirectionRight)
        [self.tabBarController setSelectedIndex:[self.tabBarController selectedIndex] - 1];
}

-(void) swipeLeft:(UISwipeGestureRecognizer *) recognizer {
    if (recognizer.direction == UISwipeGestureRecognizerDirectionLeft)
        [self.tabBarController setSelectedIndex:[self.tabBarController selectedIndex] + 1];
    
}



@end
