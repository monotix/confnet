//
//  Answer.h
//  Confnet
//
//  Created by Rafał Osowicki on 03.08.2015.
//  Copyright (c) 2015 Rafał Osowicki. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Answer : NSManagedObject

@property (nonatomic, retain) NSString * answer;
@property (nonatomic, retain) NSString * questionID;
@property (nonatomic, retain) NSString * userID;

@end
