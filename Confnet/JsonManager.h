//
//  JsonManager.h
//  Confnet
//
//  Created by Rafał Osowicki on 06.07.2015.
//  Copyright (c) 2015 Rafał Osowicki. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>

@interface JsonManager : NSObject{
    NSString *address;
    NSString *token;
    NSString *err;
    int programListCount;
    int quCount;
    int valCount;
    int counter;
    int meetingsCount;
    
}

@property(nonatomic, strong) NSArray *JsonMsg;
@property(nonatomic, strong) NSArray *JsonMsgDetail;
@property(nonatomic, strong) NSArray *questionnaire;
@property(nonatomic, strong) NSArray *programs;
@property(nonatomic, strong) NSDate *fullDate;
@property(nonatomic, strong) NSString *dateTitle;
@property int programListCount;


-(id)initWithAddress:(NSString*)addr;
-(void)getJSONByAddress:(NSString*)addr Completion:(void(^)(BOOL success))completion;
-(void)getJSONByAddress:(NSString*)addr Login:(NSString*)login Password:(NSString* )pass Completion:(void(^)(BOOL success))completion;

-(NSString*)getToken;
-(void)getJSONByAddress:(NSString *)addr WithFacebookToken:(NSString *)tkn Completion:(void (^)(BOOL))completion;
-(void)getJSONByAddress:(NSString *)addr WithLinkedinToken:(NSString *)tkn Completion:(void (^)(BOOL))completion;

//Participians
-(void)getJSONByAddress:(NSString*)addr WithToken:(NSString*)tkn Completion:(void(^)(BOOL success))completion;
-(NSString*)getCompanyAtIndex:(int)index;
-(NSString*)getUserNameAtIndex:(int)index;
-(NSInteger* )getUserCount;
-(NSString*)getUserIDAtIndex:(int)index;
-(NSString*)getAvatarURLAtIndex:(int)index;

//Meetings
-(void)getJSONMeetingsByAddress:(NSString *)addr WithToken:(NSString *)tkn Completion:(void (^)(BOOL))completion;
-(int)getMeetingId:(int)index;
-(int)getMeetingsCount;
-(NSString*)getInviteState:(int)index;
-(NSDate*)getTime:(int)index;
-(NSString*)getMeetingUserId:(int)index;
-(NSString *)getUserName;
-(NSString *)getUserCompany;
-(NSString *)getUserAvatar;
-(NSString *)getUserPaid;
-(NSString *)getMessage:(int)index;


//Participians detail
-(void)getJSONByAddress:(NSString *)addr WithToken:(NSString *)tkn UserID:(NSString *)uid Completion:(void (^)(BOOL))completion;
-(NSString *)getcompanyPosition;
-(NSString *)getUserEmail;
-(NSString *)getUserPhone1;
-(NSString *)getUserPhone2;
-(NSString *)getcompany;
-(int)getQuestCount;
-(NSString *)getDescriptionAtIndex:(int)index;
-(NSArray *)getValueArIndex:(int)index;

//Program
-(NSDate*)getFullDateAtIndex:(int)index;
-(NSString*)getDayHumanAtIndex:(int)index;
-(int)getDayCount;
-(int)getDayProgramCount:(int)day;
-(NSString*)getHourInDay:(int)day AtIndex:(int)index;
-(NSString* )getDetailValueInDay:(int)day AtIndex:(int)index;

-(NSString* )getError;




@end
