//
//  DBProgram.h
//  Confnet
//
//  Created by Rafał Osowicki on 03.08.2015.
//  Copyright (c) 2015 Rafał Osowicki. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface DBProgram : NSManagedObject

@property (nonatomic, retain) NSDate * dateProgram;
@property (nonatomic, retain) NSString * dayHuman;

@end
