//
//  Question.m
//  Confnet
//
//  Created by Rafał Osowicki on 03.08.2015.
//  Copyright (c) 2015 Rafał Osowicki. All rights reserved.
//

#import "Question.h"


@implementation Question

@dynamic answerID;
@dynamic question;
@dynamic userID;

@end
