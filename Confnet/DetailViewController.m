//
//  DetailViewController.m
//  Confnet
//
//  Created by Rafał Osowicki on 02.07.2015.
//  Copyright (c) 2015 Rafał Osowicki. All rights reserved.
//

#import "DetailViewController.h"
#import "JsonManager.h"
#import "Participants.h"
#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIView.h>
@interface DetailViewController (){
    UILabel *TopLB;
    UILabel *BottomLB;
    UILabel *UserNameLB;
    UILabel *companyLB;
    UIImageView *avatar;
    UILabel *phone1LB;
    UILabel *phone2LB;
    UILabel *emailLB;
    UILabel *valueLB;
    UILabel *titleLB;
    UILabel *value2LB;
    NSString *token;
    NSMutableArray *keys;
    NSMutableDictionary *quest;
    
    float gap;
    float gap2;
    float gap3;
    float gap4;
}

@end

@implementation DetailViewController
@synthesize participant;

- (void)loadView {
    
    CGRect fullScreenRect=[[UIScreen mainScreen] applicationFrame];
    scrollView=[[UIScrollView alloc] initWithFrame:fullScreenRect];
    //scrollView.contentSize=CGSizeMake(320,600);
    
    self.view=scrollView;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    DBM = [DBManager sharedManager];
    UIImage* image3 = [UIImage imageNamed:@"Back.png"];
    CGRect frameimg = CGRectMake(15,5, 30,30);
    UIButton *backButton = [[UIButton alloc] initWithFrame:frameimg];
    [backButton setBackgroundImage:image3 forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backToList)
           forControlEvents:UIControlEventTouchUpInside];
    [backButton setShowsTouchWhenHighlighted:YES];
    
    UIBarButtonItem *backButton1 =[[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    UINavigationItem *navItem = [[UINavigationItem alloc] init];
    navItem.leftBarButtonItem = backButton1;
    navItem.title = @"User Details";
    super.bar.items = @[ navItem ];
    
    _screenBound = [[UIScreen mainScreen] bounds];
    
    NSLog(@"%@ %@ %@ %@",participant.email,participant.phone2,participant.avatarURL,participant.companyPosition);
    self.view.backgroundColor = [UIColor colorWithRed:0.99 green:0.99 blue:0.99 alpha:1.0];
    gap=150;
    
    CGRect contetntTopRectangle = CGRectMake(_screenBound.size.width*0.05, _screenBound.size.height*0.20, _screenBound.size.width*0.95- _screenBound.size.width*0.05, 150);
    TopLB = [[UILabel alloc] initWithFrame:contetntTopRectangle];
    TopLB.font = [UIFont systemFontOfSize:12];
    TopLB.textColor = [UIColor grayColor];
    TopLB.layer.shadowOffset = CGSizeMake(1, 0);
    TopLB.layer.shadowColor = [[UIColor blackColor] CGColor];
    TopLB.layer.shadowRadius = 2;
    TopLB.layer.shadowOpacity = .25;
    TopLB.layer.backgroundColor = [UIColor whiteColor].CGColor;
    [self.view addSubview: TopLB];
    
    CGRect cBounds = contetntTopRectangle;
    avatar = [[UIImageView alloc] initWithFrame:CGRectMake(_screenBound.size.width*0.37, _screenBound.size.height*0.15, 100, 100)];
    if([DBMa hasConnectivity]){
    
    NSURL *url = [NSURL URLWithString:participant.avatarURL];
    NSData *data = [NSData dataWithContentsOfURL : url];
    UIImage *image = [UIImage imageWithData: data];
    avatar.image = image;
    }else{
        NSLog(@"imggg2: %@", imageAV);
        avatar.image = imageAV;
    }
    
    avatar.layer.cornerRadius = avatar.frame.size.width / 2;
    avatar.clipsToBounds = YES;
    [self.view addSubview: avatar];
    
    CGRect userNameTopRectangle = CGRectMake(cBounds.origin.x+10, cBounds.origin.y+80, cBounds.size.width*0.7, 25);
    UserNameLB = [[UILabel alloc] initWithFrame:userNameTopRectangle];
    //_nameUserLB.font = [UIFont boldSystemFontOfSize:25];
    UserNameLB.font = [UIFont systemFontOfSize:18];
    UserNameLB.textColor = [UIColor colorWithRed:91.0f/255.0f green:1.0f/255.0f blue:200.0f/255.0f alpha:1.0];
    UserNameLB.layer.backgroundColor = [UIColor clearColor].CGColor;
    if(![participant.userName isEqualToString:@"<null>"])
        UserNameLB.text = participant.userName;
    [self.view addSubview: UserNameLB];
    
    CGRect companyTopRectangle = CGRectMake(cBounds.origin.x+10, cBounds.origin.y+100, cBounds.size.width*0.7, 45);
    companyLB = [[UILabel alloc] initWithFrame:companyTopRectangle];
    //_nameUserLB.font = [UIFont boldSystemFontOfSize:25];
    companyLB.font = [UIFont systemFontOfSize:12];
    companyLB.textColor = [UIColor grayColor];
    companyLB.layer.backgroundColor = [UIColor clearColor].CGColor;
    companyLB.lineBreakMode = NSLineBreakByWordWrapping;
    companyLB.numberOfLines = 0;
    
    NSLog(@"POsition %@", participant.companyPosition);
    
    if(!(participant.company == nil)){
        companyLB.text = [NSString stringWithFormat:@"%@",participant.company];
        NSLog(@"tutaj %@", participant.company);
    }
    if(![participant.companyPosition isEqualToString:@"<null>"] && ![participant.companyPosition isEqualToString:@"(null)"] && !(participant.companyPosition == nil) && ![participant.companyPosition isEqualToString:@""])
            companyLB.text = [NSString stringWithFormat:@"%@, %@",participant.companyPosition,participant.company];
    

    [self.view addSubview: companyLB];
    
    if(![self isNullOrEmpty:participant.phone1] && ![participant.phone1 isEqual:[NSNull class]] && ![participant.phone1 isEqualToString:@"<null>"]){
    CGRect phone1Rectangle = CGRectMake(cBounds.origin.x, cBounds.origin.y+gap, cBounds.size.width, 40);
    phone1LB = [[UILabel alloc] initWithFrame:phone1Rectangle];
    phone1LB.layer.shadowOffset = CGSizeMake(1, 0);
    phone1LB.layer.shadowColor = [[UIColor blackColor] CGColor];
    phone1LB.layer.shadowRadius = 2;
    phone1LB.layer.shadowOpacity = .25;
    phone1LB.font = [UIFont systemFontOfSize:12];
    phone1LB.textColor = [UIColor grayColor];
    phone1LB.layer.backgroundColor = [UIColor whiteColor].CGColor;
    phone1LB.text = [NSString stringWithFormat:@"             %@",participant.phone1] ;
        
        phone1LB.userInteractionEnabled = YES;
        UITapGestureRecognizer *tapGesture =
        [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(callPhone1:)];
        [phone1LB addGestureRecognizer:tapGesture];
        
    [self.view addSubview: phone1LB];
        
        UIImageView *emailIMG =[[UIImageView alloc] initWithFrame:CGRectMake(cBounds.origin.x+15, cBounds.origin.y+gap+10, 18, 18)];
        emailIMG.image=[UIImage imageNamed:@"phone1.png"];
        [self.view addSubview:emailIMG];
        gap += 40;
    }
    if (![self isNullOrEmpty:participant.phone2] && ![participant.phone2 isEqual:[NSNull class]] && ![participant.phone2 isEqualToString:@"<null>"]){
    CGRect phone2Rectangle = CGRectMake(cBounds.origin.x, cBounds.origin.y+gap, cBounds.size.width, 40);
    phone2LB = [[UILabel alloc] initWithFrame:phone2Rectangle];
    phone2LB.layer.shadowOffset = CGSizeMake(1, 0);
    phone2LB.layer.shadowColor = [[UIColor blackColor] CGColor];
    phone2LB.layer.shadowRadius = 2;
    phone2LB.layer.shadowOpacity = .25;
    phone2LB.font = [UIFont systemFontOfSize:12];
    phone2LB.textColor = [UIColor grayColor];
    phone2LB.layer.backgroundColor = [UIColor whiteColor].CGColor;
    phone2LB.text = [NSString stringWithFormat:@"             %@",participant.phone2] ;
        
        phone2LB.userInteractionEnabled = YES;
        UITapGestureRecognizer *tapGesture =
        [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(callPhone2:)];
        [phone2LB addGestureRecognizer:tapGesture];
        
    [self.view addSubview: phone2LB];
        
        UIImageView *emailIMG =[[UIImageView alloc] initWithFrame:CGRectMake(cBounds.origin.x+15, cBounds.origin.y+gap+10, 18, 18)];
        emailIMG.image=[UIImage imageNamed:@"phone2.png"];
        [self.view addSubview:emailIMG];
        gap += 40;
    }
    if (![self isNullOrEmpty:participant.email] && ![participant.email isEqualToString:@"<null>"] && ![participant.email isEqualToString:@"<null>"]){
    CGRect emailRectangle = CGRectMake(cBounds.origin.x, cBounds.origin.y+gap, cBounds.size.width, 40);
    emailLB = [[UILabel alloc] initWithFrame:emailRectangle];
    emailLB.layer.shadowOffset = CGSizeMake(1, 0);
    emailLB.layer.shadowColor = [[UIColor blackColor] CGColor];
    emailLB.layer.shadowRadius = 2;
    emailLB.layer.shadowOpacity = .25;
    emailLB.font = [UIFont systemFontOfSize:12];
    emailLB.textColor = [UIColor grayColor];
    emailLB.layer.backgroundColor = [UIColor whiteColor].CGColor;
    emailLB.text = [NSString stringWithFormat:@"              %@",participant.email] ;
        
        emailLB.userInteractionEnabled = YES;
        UITapGestureRecognizer *tapGesture =
        [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sendEmail:)];
        [emailLB addGestureRecognizer:tapGesture];
        
        [self.view addSubview: emailLB];
        
        UIImageView *emailIMG =[[UIImageView alloc] initWithFrame:CGRectMake(cBounds.origin.x+15, cBounds.origin.y+gap+10, 18, 18)];
        emailIMG.image=[UIImage imageNamed:@"email.png"];
        [self.view addSubview:emailIMG];
        
        gap += 40;
    }
    
    //NSLog(@"czy null %@ %@",[participant.questionnaire count],participant.questionnaire);
    
    if(!([participant.questionnaire count] == nil)){
        
        gap3 = 0;
        gap4 = 0;
        CGRect contetntBtRectangle = CGRectMake(cBounds.origin.x, (cBounds.origin.y+150)+(gap-100), cBounds.size.width, 200);
    
        CGRect BtBounds = contetntBtRectangle;
    
    
        CGRect titleBtRectangle = CGRectMake(BtBounds.origin.x, BtBounds.origin.y-40, 100, 30);
        titleLB = [[UILabel alloc] initWithFrame:titleBtRectangle];
        //_nameUserLB.font = [UIFont boldSystemFontOfSize:25];
        titleLB.font = [UIFont systemFontOfSize:20];
        titleLB.textColor = [UIColor colorWithRed:91.0f/255.0f green:1.0f/255.0f blue:200.0f/255.0f alpha:1.0];
        titleLB.layer.backgroundColor = [UIColor whiteColor].CGColor;
        titleLB.text = @"Interests";
        [self.view addSubview:titleLB];
        
        //keys = [keys sortedArrayUsingComparator:^(id a, id b) {
         //   return [a compare:b options:NSNumericSearch];
        //}];
       
        
        for (int i = 0; i < [participant.questionnaire count]; i++) {
            
            CGRect valueBtRectangle = CGRectMake(BtBounds.origin.x+10, BtBounds.origin.y+gap2+gap3+gap4, BtBounds.size.width*0.35, 60);
            valueLB = [[UILabel alloc] initWithFrame:valueBtRectangle];
            valueLB.font = [UIFont systemFontOfSize:15];
            valueLB.textColor = [UIColor grayColor];
            valueLB.lineBreakMode = NSLineBreakByWordWrapping;
            valueLB.numberOfLines = 0;
            valueLB.textAlignment = NSTextAlignmentCenter;
            value2LB.adjustsFontSizeToFitWidth = YES;            //[valueLB sizeToFit];
            valueLB.text = [participant.keys objectAtIndex:i];
            
            NSArray *tab = [NSArray arrayWithObjects:[participant.questionnaire valueForKey:[participant.keys objectAtIndex:i]], nil];
            //NSLog(@"tab: %@, %d",tab,[[tab objectAtIndex:0] count]);
            if (![[tab objectAtIndex:0] isKindOfClass:[NSString class]]){
                for (int j = 0; j < [[tab objectAtIndex:0] count]; j++) {
                    CGRect value2BtRectangle = CGRectMake(BtBounds.origin.x+BtBounds.size.width*0.43, BtBounds.origin.y+gap2+gap4+gap3+20, BtBounds.size.width*0.5, 25);
                    value2LB = [[UILabel alloc] initWithFrame:value2BtRectangle];
                    value2LB.font = [UIFont systemFontOfSize:12];
                    value2LB.textColor = [UIColor grayColor];
                    value2LB.adjustsFontSizeToFitWidth = NO;
                    value2LB.lineBreakMode = NSLineBreakByWordWrapping;
                    value2LB.numberOfLines = 0;
                    //value2LB.lineBreakMode = UILineBreakModeTailTruncation;
                    value2LB.text = [NSString stringWithFormat:@"\u2022  %@",[[tab objectAtIndex:0] objectAtIndex:j]];
                
                    gap4 += 15;
                    [self.view addSubview: value2LB];
                }
            }else{
                CGRect value2BtRectangle;
                if ([[participant.keys objectAtIndex:i]isEqualToString:@"About company"]){
                    value2BtRectangle = CGRectMake(BtBounds.origin.x+BtBounds.size.width*0.43, BtBounds.origin.y+gap2+gap4+gap3+20, BtBounds.size.width*0.5, 170);
                    gap4 += 145;
                    
                }else{
                    value2BtRectangle = CGRectMake(BtBounds.origin.x+BtBounds.size.width*0.43, BtBounds.origin.y+gap2+gap4+gap3+20, BtBounds.size.width*0.5, 25);
                }
                
                value2LB = [[UILabel alloc] initWithFrame:value2BtRectangle];
                //value2LB.textAlignment = NSTextAlignmentCenter;
                value2LB.font = [UIFont systemFontOfSize:12];
                value2LB.textColor = [UIColor grayColor];
                NSString *temp = [NSString stringWithFormat:@"\u2022  %@",[tab objectAtIndex:0]];
                value2LB.lineBreakMode = NSLineBreakByWordWrapping;
                value2LB.numberOfLines = 0;
                //[value2LB sizeToFit];
                value2LB.text = temp;
                
                gap4 += 15;
                [self.view addSubview: value2LB];
                
            }
    
            UIView *line = [[UIView alloc] initWithFrame:CGRectMake(BtBounds.origin.x, BtBounds.origin.y+60+gap2+gap3+gap4-10, BtBounds.size.width, 0.5f)];
            line.backgroundColor = [UIColor colorWithRed:(200.0f/255.0f) green:(200.0f/255.0f) blue:(200.0f/255.0f) alpha:1.0f];
            line.layer.shadowColor = [UIColor darkGrayColor].CGColor;
            line.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);
            line.layer.shadowRadius = 0.5f;
            line.layer.shadowOpacity = 0.4f;
            line.layer.masksToBounds = NO;
            
            gap3 += 60;
            
            [self.view addSubview: valueLB];
            if (i < [participant.questionnaire count]-1) {
                [self.view addSubview:line];
            }
            
        }
    
        CGRect rec = CGRectMake(cBounds.origin.x, (cBounds.origin.y+150)+(gap-100), cBounds.size.width, gap2+gap3+gap4);
        BottomLB = [[UILabel alloc] initWithFrame:rec];
        //_nameUserLB.font = [UIFont boldSystemFontOfSize:25];
        BottomLB.font = [UIFont systemFontOfSize:12];
        BottomLB.textColor = [UIColor grayColor];
        BottomLB.layer.shadowOffset = CGSizeMake(1, 0);
        BottomLB.layer.shadowColor = [[UIColor blackColor] CGColor];
        BottomLB.layer.shadowRadius = 4;
        BottomLB.layer.shadowOpacity = .25;
        BottomLB.layer.backgroundColor = [UIColor whiteColor].CGColor;
        [self.view addSubview:BottomLB];
        [self.view sendSubviewToBack: BottomLB];
        
        scrollView.contentSize = CGSizeMake(300, gap+gap2+gap3+gap4+175);
    }
    
}

-(void)viewDidAppear:(BOOL)animated{
    
    _activityindicator = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(150, 200, 30, 30)];
    [_activityindicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [_activityindicator setColor:[UIColor colorWithRed:217.0f/255.0f green:11.0f/255.0f blue:140.0f/255.0f alpha:1.0]];
    _activityindicator.center = self.view.center;
    [self.view addSubview:_activityindicator];
    if([DBMa hasConnectivity]){
   
        if ([participant.email isEqualToString:@""] || participant.email == nil){
            [_activityindicator startAnimating];
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            [self detailParticipant];
            });
        }
    }
}

- (IBAction)callPhone1:(id)sender{
    //phone1LB.backgroundColor =[UIColor colorWithRed:0.99 green:0.99 blue:0.99 alpha:1.0];
    NSString *number = [NSString stringWithFormat:@"%@",participant.phone1];
    NSString *myNewString = [number stringByReplacingOccurrencesOfString:@"\\s"
                                                              withString:@""
                                                                 options:NSRegularExpressionSearch
                                                                   range:NSMakeRange(0, [number length])];
    NSLog(@"%@",myNewString);
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt:%@", myNewString]]];
    NSLog(@"Call1");
}

- (IBAction)callPhone2:(id)sender{
    
    NSString *number = [NSString stringWithFormat:@"%@",participant.phone2];
    NSString *myNewString = [number stringByReplacingOccurrencesOfString:@"\\s"
                                                                withString:@""
                                                                   options:NSRegularExpressionSearch
                                                                     range:NSMakeRange(0, [number length])];
    NSLog(@"%@",myNewString);
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt:%@", myNewString]]];
}

-(IBAction)sendEmail:(id)sender{
    
    if (![DBMa hasConnectivity]) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Network" message: @"No internet connection." delegate: self
                                             cancelButtonTitle:@"OK"
                                             otherButtonTitles:nil,nil];
        
        [alert show];
        return;
    }
    
    emailLB.backgroundColor =[UIColor colorWithRed:0.99 green:0.99 blue:0.99 alpha:1.0];
    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
    if (mailClass != nil) {
        
        MFMailComposeViewController *mailcontroller = [[MFMailComposeViewController alloc] init];
        mailcontroller.mailComposeDelegate = self;
        [mailcontroller setMailComposeDelegate:self];
        NSArray *emailArray = [[NSArray alloc] initWithObjects:participant.email, nil];
        [mailcontroller setToRecipients:emailArray];
        //[self presentViewController:mailcontroller animated:YES completion:nil];
        
        if([mailClass canSendMail]) {
            [self presentModalViewController:mailcontroller animated:YES];
        }
    }
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    NSString *alertMsg;
    
    switch (result) {
        case MFMailComposeResultSent:
            NSLog(@"You sent the email.");
            alertMsg = [NSString stringWithFormat:@"You sent the email."];
            break;
        case MFMailComposeResultSaved:
            NSLog(@"You saved a draft of this email");
            alertMsg = [NSString stringWithFormat:@"You saved a draft of this email"];
            break;
        case MFMailComposeResultCancelled:
            NSLog(@"You cancelled sending this email.");
            alertMsg = [NSString stringWithFormat:@"You cancelled sending this email."];
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed:  An error occurred when trying to compose this email");
            alertMsg = [NSString stringWithFormat:@"Mail failed:  An error occurred when trying to compose this email"];
            break;
        default:
            NSLog(@"An error occurred when trying to compose this email");
            alertMsg = [NSString stringWithFormat:@"An error occurred when trying to compose this email"];
            break;
    }
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Email" message: alertMsg delegate: self
                                         cancelButtonTitle:@"OK"
                                         otherButtonTitles:nil,nil];
    
    [alert show];
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}


-(void)setParticipant:(Participants*)par AndIndex:(int)i Image:(UIImage *)img PartList:(NSMutableArray *)PL{

    DBM = [DBManager sharedManager];
    if(![participant isEqual:par])
        participant = [[Participants alloc] initWithParticipant:par];
    
    if(![participant isEqual:par])
        ParticipantsList = [[NSMutableArray alloc] init];
        ParticipantsList = [DBM loadParticipantsFromDB];
    
    if(i != index)
        index = i;
    NSLog(@"img: %@", img);
    imageAV = [[UIImage alloc] init];
    imageAV = [DBM loadImage:[NSString stringWithFormat:@"%@.png",[par usrID]]];
    NSLog(@"img11: %@", imageAV);
    
}

-(void)detailParticipant{
    //[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSString *address2 = [NSString stringWithFormat:@"http://polanditmeeting.com/wp-json/cn/participants/detail/"];
    token = [DBMa loadTokenFromDB];
    JManager = [[JsonManager alloc] init];
    [JManager getJSONByAddress:address2 WithToken:token UserID:participant.usrID Completion:^(BOOL success) {
        if (success) {
            
        
        participant.email = [JManager getUserEmail];
        participant.phone1 = [JManager getUserPhone1];
        participant.phone2 = [JManager getUserPhone2];
        participant.companyPosition = [JManager getcompanyPosition];
            participant.company = [JManager getcompany];
            
        quest = [[NSMutableDictionary alloc] init];
        keys = [[NSMutableArray alloc] init];
        for (int j = 0; j < [JManager getQuestCount]; j++) {
            [quest setObject:[JManager getValueArIndex:j] forKey:[JManager getDescriptionAtIndex:j]];
            [keys addObject:[JManager getDescriptionAtIndex:j]];
        }
        participant.questionnaire = quest;
        participant.keys = keys;
            [ParticipantsList replaceObjectAtIndex:index withObject:participant];
            [DBM saveParticipantsToDB:ParticipantsList];
            [self viewDidLoad];
            //[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            [_activityindicator stopAnimating];
        }else{
                
        }
            
    }];

     
}


-(BOOL)isNullOrEmpty:(NSString *)x {
    BOOL retVal = YES;
    
    if(x != nil)
    {
        if( [x isKindOfClass:[NSString class]] )
        {
            retVal = x.length == 0;
        }
        else
        {
            NSLog(@"isNullOrEmpty, value not a string");
        }
    }
    return retVal;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
