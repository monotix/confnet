//
//  Program.h
//  Confnet
//
//  Created by Rafał Osowicki on 06.07.2015.
//  Copyright (c) 2015 Rafał Osowicki. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Program : NSObject{
    NSDate *dayDateObj;
    NSString *day_human;
    NSString *hours;
    NSString *detailValue;
}
@property(nonatomic, retain) NSDate *dayDateObj;
@property(nonatomic, retain) NSString *day_human;
@property(nonatomic, retain) NSMutableArray *hours;
@property(nonatomic, retain) NSMutableArray *detailValue;

-(id)initWithFullDay:(NSDate*)fd DayHuman:(NSString*)d Hours:(NSMutableArray *)hour DetailValue:(NSMutableArray *)detail;
-(id)initWithProgram:(Program*)program;
@end
