//
//  QuestionAnswer.m
//  Confnet
//
//  Created by Rafał Osowicki on 03.08.2015.
//  Copyright (c) 2015 Rafał Osowicki. All rights reserved.
//

#import "QuestionAnswer.h"
#import "Participant.h"
#import "ValueAnswer.h"


@implementation QuestionAnswer

@dynamic answerQuestion;
@dynamic answerID;
@dynamic partycipantID;

@end
