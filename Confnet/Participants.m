//
//  Participants.m
//  Confnet
//
//  Created by Rafał Osowicki on 08.07.2015.
//  Copyright (c) 2015 Rafał Osowicki. All rights reserved.
//

#import "Participants.h"

@implementation Participants

@synthesize userName,usrID,email,company,companyPosition,phone1,phone2,userPaid,avatarURL, questionnaire;

-(id)initWithUserName:(NSString* )un UserID:(NSString*)uid Email:(NSString*)e Company:(NSString*)c CompanyPosition:(NSString*)cp Phone1:(NSString*)p1 Phone2:(NSString*)p2 UserPaid:(BOOL)up AvatarURL:(NSString*)av Questionnaire:(NSDictionary *)q Keys:(NSMutableArray *)k{
    
    if(self){
        self.userName = un;
        self.usrID = uid;
        self.email = e;
        self.company=c;
        self.companyPosition=cp;
        self.phone1 = p1;
        self.phone2 = p2;
        self.userPaid = up;
        self.avatarURL = av;
        self.questionnaire = q;
        self.keys = k;

    }
    
    return self;
}

-(id)initWithParticipant:(Participants*)src{
    
    if(self){
        self.userName = src.userName;
        self.usrID = src.usrID;
        self.email = src.email;
        self.company=src.company;
        self.companyPosition=src.companyPosition;
        self.phone1 = src.phone1;
        self.phone2 = src.phone2;
        self.userPaid = src.userPaid;
        self.avatarURL = src.avatarURL;
        self.questionnaire = src.questionnaire;
        self.keys = src.keys;
    }
    
    return self;
}

@end

