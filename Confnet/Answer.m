//
//  Answer.m
//  Confnet
//
//  Created by Rafał Osowicki on 03.08.2015.
//  Copyright (c) 2015 Rafał Osowicki. All rights reserved.
//

#import "Answer.h"


@implementation Answer

@dynamic answer;
@dynamic questionID;
@dynamic userID;

@end
