//
//  Participants.h
//  Confnet
//
//  Created by Rafał Osowicki on 08.07.2015.
//  Copyright (c) 2015 Rafał Osowicki. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Participants : NSObject{
    NSString *userName;
    NSString *userID;
    NSString *email;
    NSString *company;
    NSString *companyPosition;;
    NSString *phone1;
    NSString *phone2;
    BOOL userPaid;
    NSString *avatarURL;
    NSDictionary *keys;
    NSDictionary *questionnaire;
}

@property(nonatomic, retain) NSString *userName;
@property(nonatomic, retain) NSString *usrID;
@property(nonatomic, retain) NSString *email;
@property(nonatomic, retain) NSString *company;
@property(nonatomic, retain) NSString *companyPosition;
@property(nonatomic, retain) NSString *phone1;
@property(nonatomic, retain) NSString *phone2;
@property(nonatomic, assign) BOOL userPaid;
@property(nonatomic, retain) NSString *avatarURL;
@property(nonatomic, retain) NSMutableArray *keys;
@property(nonatomic, retain) NSDictionary *questionnaire;

-(id)initWithUserName:(NSString* )un UserID:(NSString*)uid Email:(NSString*)e Company:(NSString*)c CompanyPosition:(NSString*)cp Phone1:(NSString*)p1 Phone2:(NSString*)p2 UserPaid:(BOOL)up AvatarURL:(NSString*)av Questionnaire:(NSDictionary *)q Keys:(NSMutableArray *)k;
-(id)initWithParticipant:(Participants*)src;


@end
