//
//  Meeting.h
//  Confnet
//
//  Created by Rafał Osowicki on 13.07.2015.
//  Copyright (c) 2015 Rafał Osowicki. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Meeting : NSObject{
    int mid;
    NSString *inviteState;
    NSDate *time;
    NSString *msg;
    NSString *usrID;
}

@property(nonatomic, retain) NSString *inviteState;
@property(nonatomic, retain) NSString *msg;
@property(nonatomic, retain) NSString *usrID;
@property(nonatomic, retain) NSDate *time;
@property int mid;


-(id)initWithMid:(int)m InviteState:(NSString*)is Time:(NSDate*)t Message:(NSString*)msgs UserID:(NSString *)ui;
-(id)initWithMeeting:(Meeting *)src;
@end
