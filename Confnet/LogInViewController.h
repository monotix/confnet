//
//  LogInViewController.h
//  Confnet
//
//  Created by Rafał Osowicki on 02.07.2015.
//  Copyright (c) 2015 Rafał Osowicki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BarMenuViewController.h"
#import "DBManager.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
//#import "Reachability.h"
#import <SystemConfiguration/SystemConfiguration.h>
#import <sys/socket.h>
#import <netinet/in.h>


@class  JsonManager;
@interface LogInViewController : BarMenuViewController<FBSDKLoginButtonDelegate>{
    JsonManager *JManager;
    DBManager *DBM;
    UIScrollView *scrollView;
}

//@property (strong, nonatomic) JsonManager *JManager;
@property (weak, nonatomic) IBOutlet UITextField *LoginTF;
@property (weak, nonatomic) IBOutlet UITextField *PasswordTF;
@property (weak, nonatomic) IBOutlet UILabel *LoginLB;
@property (weak, nonatomic) IBOutlet UILabel *LoginFBLB;
@property (weak, nonatomic) IBOutlet UILabel *LoginLinLB;
@property (weak, nonatomic) NSString *login;
@property (weak, nonatomic) NSString *password;
@property(nonatomic) CGRect screenBound;

-(BOOL)isNullOrEmpty:(NSString *)x;
@end
