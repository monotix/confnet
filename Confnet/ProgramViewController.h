//
//  ProgramViewController.h
//  Confnet
//
//  Created by Rafał Osowicki on 02.07.2015.
//  Copyright (c) 2015 Rafał Osowicki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BarMenuViewController.h"
#include "JsonManager.h"
#include "DBManager.h"

@interface ProgramViewController : BarMenuViewController<UITableViewDataSource, UITableViewDelegate>{
    JsonManager *JManager;
    UIRefreshControl *refreshControl;
    int programCount;
    int dayProgramCount;
    UIScrollView *scrollView;
    BOOL ListDone;
    DBManager *DBM;
}

@property (strong, nonatomic) UITableView *tableView;
@property (nonatomic, strong) UIActivityIndicatorView *activityindicator;

-(void)ListInit;
-(void)refreshData:(UITableView *)t;
- (void)refreshTable;
@end
