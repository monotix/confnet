//
//  TabBarDelegate.m
//  Confnet
//
//  Created by Rafał Osowicki on 15.07.2015.
//  Copyright (c) 2015 Rafał Osowicki. All rights reserved.
//

#import "TabBarDelegate.h"
#import "ParticipantsViewController.h"
@class ParticipantsViewController;
@implementation TabBarDelegate

-(void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController{
    
    NSLog(@"Delegat: %d",[tabBarController selectedIndex]);
    int sel = [tabBarController selectedIndex];
    [[[[tabBarController tabBar] items] objectAtIndex:sel] setEnabled:NO];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [[[[tabBarController tabBar] items] objectAtIndex:sel] setEnabled:YES];
    });
    
}

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item{
    
    NSLog(@"item %@", item);
}


@end
