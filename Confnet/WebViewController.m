//
//  WebViewController.m
//  Confnet
//
//  Created by Rafał Osowicki on 22.07.2015.
//  Copyright (c) 2015 Rafał Osowicki. All rights reserved.
//

#import "WebViewController.h"

@interface WebViewController ()

@end

@implementation WebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    DBM = [DBManager sharedManager];
    JManager = [[JsonManager alloc] init];
    NSURL *url = [NSURL URLWithString:@"https://www.linkedin.com/uas/oauth2/authorization?response_type=code&client_id=75p9drxjcrpjq5&redirect_uri=http%3A%2F%2Fpolanditmeeting.com&state=123654789321456987&scope=r_emailaddress"];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [_webView loadRequest:request];
    
    
}

-(void)webViewDidStartLoad:(UIWebView *)webView
{
    NSLog(@"%@", [_webView.request mainDocumentURL]);
    NSURL *url = [_webView.request mainDocumentURL];
    urlLink = [NSString stringWithFormat:@"%@",[url absoluteString]];
    NSLog(@"link: %@",urlLink);
   
    
    if([urlLink isEqualToString:@"http://polanditmeeting.com/?error=access_denied&error_description=the+user+denied+your+request&state=123654789321456987"])
    {
        [self performSegueWithIdentifier:@"webOut" sender:self];
        return;
    }
    
    if(![url class] == nil){
        
        NSString *part1 = [[urlLink componentsSeparatedByString:@"="] objectAtIndex:1];
        token = [[part1 componentsSeparatedByString:@"&"] objectAtIndex:0];
    
        [JManager getJSONByAddress:@"http://polanditmeeting.com/wp-json/cn/auth/linkedin" WithLinkedinToken:token Completion:^(BOOL success) {
        
            if(success){
                
                [DBM saveTokenToDB:[JManager getToken]];
                [self performSegueWithIdentifier:@"webOut" sender:self];
                
            }else{
                
                
                
            }
            
        }];
    
    }

}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    
    //NSLog(@"%@",[_webView.request mainDocumentURL]);
    NSURL *url = [_webView.request mainDocumentURL];
    urlLink = [NSString stringWithFormat:@"%@",[url absoluteString]];
    NSLog(@"%ld",(long)[error code]);
    if([error code] == -1009){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Network" message: @"No internet connection." delegate: self
                                             cancelButtonTitle:@"OK"
                                             otherButtonTitles:nil,nil];
        
        [alert show];
        [self performSegueWithIdentifier:@"webOut" sender:self];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
