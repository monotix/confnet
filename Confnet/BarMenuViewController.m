//
//  BarMenuViewController.m
//  Confnet
//
//  Created by Rafał Osowicki on 02.07.2015.
//  Copyright (c) 2015 Rafał Osowicki. All rights reserved.
//

#import "BarMenuViewController.h"
#import "LogInViewController.h"
#import "AppDelegate.h"
#import <CoreData/CoreData.h>

@interface BarMenuViewController ()

@end

@implementation BarMenuViewController
@synthesize bar;

- (void)viewDidLoad {
    [super viewDidLoad];
   
    bar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 60)];
    [bar setBarTintColor:[UIColor colorWithRed:53.0f/255.0f green:5.0f/255.0f blue:132.0f/255.0f alpha:1.0]];
    bar.tintColor = [UIColor whiteColor];
    [bar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor colorWithRed:0.97 green:0.96 blue:0.98 alpha:1.0]}];
    //UIBarButtonItem * item = [[UIBarButtonItem alloc] initWithCustomView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ic_action_logout.png"]]];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Logout" style:UIBarButtonItemStylePlain target:self action:@selector(Logout)];
    UINavigationItem *navItem = [[UINavigationItem alloc] init];
    navItem.rightBarButtonItem = backButton;
    //navItem.hidesBackButton = YES;
    //bar.items = @[ navItem ];
    [self.view addSubview:bar];
    
    DBMa = [DBManager sharedManager];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



-(void)Logout{
    
    [DBMa deleteToken];
    [DBMa deleteProgramFromDB];
    [DBMa deleteParticipanFromDB];
    [DBMa deleteMeetingsFromDB];
    [DBMa removeImg];
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logOut];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    LogInViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"login"];
    [topController presentViewController:vc animated:YES completion:nil];
}

-(void)backToList{
    
   [self dismissModalViewControllerAnimated:YES];
}




@end
