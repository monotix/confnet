//
//  ConntactsViewController.h
//  Confnet
//
//  Created by Rafał Osowicki on 02.07.2015.
//  Copyright (c) 2015 Rafał Osowicki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BarMenuViewController.h"
#import <MessageUI/MessageUI.h>

@interface ConntactsViewController : BarMenuViewController<MFMailComposeViewControllerDelegate>{
    UIScrollView *scrollView;
}

@property(nonatomic) CGRect screenBound;
@property(nonatomic,assign) id<MFMailComposeViewControllerDelegate> mailComposeDelegate;
@end
