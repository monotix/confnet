//
//  ParticipantsViewController.h
//  Confnet
//
//  Created by Rafał Osowicki on 02.07.2015.
//  Copyright (c) 2015 Rafał Osowicki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BarMenuViewController.h"
#import "JsonManager.h"
#import "DBManager.h"

@class JsonManager;
@interface ParticipantsViewController : BarMenuViewController<UITableViewDataSource, UITableViewDelegate>{
    
    JsonManager *JManager;
    DBManager *DBM;
    NSMutableArray *ParticipantsList;
    NSString *uid;
    NSMutableArray *imageArray;
    UITableView *tableView;
    NSMutableDictionary *quest;
    NSMutableArray *keys;
    NSMutableArray *keyList;
    int *userCount;
    BOOL ListDone;
    BOOL allImg;
    BOOL refresh;
    BOOL sc;
    BOOL bg;
}

@property(nonatomic) NSString *token;
@property (strong, nonatomic) UITableView *tableView;
@property (nonatomic, strong) UIActivityIndicatorView *activityindicator;

@end
