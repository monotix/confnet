//
//  CellController.h
//  Confnet
//
//  Created by Rafał Osowicki on 03.07.2015.
//  Copyright (c) 2015 Rafał Osowicki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface CellController : UITableViewCell

@property(nonatomic) CGRect screenBound;
@property(copy, nonatomic) NSString *titleDate;
@property(copy, nonatomic) NSMutableArray *hours;
@property(copy, nonatomic) NSMutableArray *value;
@property(copy, nonatomic) NSString *userName;
@property(copy, nonatomic) NSString *company;
@property(copy, nonatomic) NSString *avatarUrl;
@property(nonatomic) UIImageView *avatarIMG;
@property(copy, nonatomic) NSDate *time;
@property(copy, nonatomic) NSString *status;



@end
