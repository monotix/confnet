//
//  MeetingsDB.h
//  Confnet
//
//  Created by Rafał Osowicki on 03.08.2015.
//  Copyright (c) 2015 Rafał Osowicki. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface MeetingsDB : NSManagedObject

@property (nonatomic, retain) NSString * message;
@property (nonatomic, retain) NSString * status;
@property (nonatomic, retain) NSDate * time;
@property (nonatomic, retain) NSString * userID;

@end
