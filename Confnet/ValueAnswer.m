//
//  ValueAnswer.m
//  Confnet
//
//  Created by Rafał Osowicki on 03.08.2015.
//  Copyright (c) 2015 Rafał Osowicki. All rights reserved.
//

#import "ValueAnswer.h"
#import "QuestionAnswer.h"


@implementation ValueAnswer

@dynamic value;
@dynamic questionID;

@end
