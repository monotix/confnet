//
//  MeetingsViewController.h
//  Confnet
//
//  Created by Rafał Osowicki on 02.07.2015.
//  Copyright (c) 2015 Rafał Osowicki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BarMenuViewController.h"
#import "JsonManager.h"
#import "DBManager.h"

@interface MeetingsViewController : BarMenuViewController<UITableViewDataSource, UITableViewDelegate>{
    UIRefreshControl *refreshControll;
    JsonManager *JManager;
    DBManager *DBM;
    NSString *token;
    NSMutableArray *MeetingsList;
    NSMutableArray *MeetingsListTemp;
    NSMutableArray *ParticipantList;
    NSMutableArray *ParticipantListTemp;
    NSMutableArray *PrtTemp;
    NSMutableArray *MeetTemp;
    NSMutableArray *muid;
    NSMutableArray *imageList;
    NSMutableDictionary *quest;
    NSMutableArray *keys;
    BOOL ListDone;
    BOOL sc;
    
}

@property (strong, nonatomic) UITableView *tableView;
@property (nonatomic, strong) UIActivityIndicatorView *activityindicator;
@property(nonatomic) CGRect screenBound;

@end
